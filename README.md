PASS INDO
==========
Git global setup

git config --global user.name "Wilie Wahyu Hidayat"

git config --global user.email "wilie.hidayat87@gmail.com"


Link a SSH key to server
==========
Check : cat ~/.ssh/id_rsa.pub

Add : ssh-keygen -t rsa -C "wilie.hidayat87@gmail.com"


Create a new repository
==========
git clone git@gitlab.com:wilie.hidayat87/pass-isat.git

cd /app/xmp2012/

touch README.md

git add README.md

git commit -m "add README"

git push -u origin master


Push an existing folder
==========
cd /app/xmp2012/

git init

git remote add origin git@gitlab.com:wilie.hidayat87/pass-isat.git

git add .

git commit -m "Initial commit"

git push -u origin master


Push an existing Git repository
==========
cd /app/xmp2012/

git remote rename origin old-origin

git remote add origin git@gitlab.com:wilie.hidayat87/pass-isat.git

git push -u origin --all

git push -u origin --tags


Pull repo to production
==========
git remote add origin git@gitlab.com:wilie.hidayat87/pass-isat.git

git fetch origin

git status

git pull -u origin master
