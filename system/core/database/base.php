<?php

class database_base {

    private static $instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function load($connProfile, $reload = FALSE) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start connProfile : " . serialize($connProfile) . " reload : " . serialize($reload)));

        $dbConfig = loader_config::getInstance()->getConfig('database');
        $profile = $dbConfig->profile[$connProfile];
        $className = 'database_' . $profile['driver']; // Change This to the right class

        if (class_exists($className)) {
            $dbClass = new $className();
            if ($reload) {
                if ($dbClass->connect($profile) === false) {
                    return false;
                }
            }
        } else {
            return false;
        }

        return $dbClass;
    }

}