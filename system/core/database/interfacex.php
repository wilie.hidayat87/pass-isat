<?php
interface database_interfacex {

	public function connect($connProfile);
	public function reconnect($connProfile);
	public function query($sql);
	public function fetch($sql);
	public function numRows();
}