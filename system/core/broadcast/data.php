<?php
class broadcast_data {
	public $id;
	public $service;
	public $operator;
	public $adn;
	public $recurringType;
	public $handlerFile;
	public $pushTime;
	public $status;
	public $contentLabel;
	public $contentSelect;
	public $lastContentId;
	public $serviceType;
	public $price;
	public $notes;
	public $created;
	public $modified;
}


