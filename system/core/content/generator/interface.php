<?php
interface content_generator_interface {
	public function generate($content_data);
	public static function getInstance();
}