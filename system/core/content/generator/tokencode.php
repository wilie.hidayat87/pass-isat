<?php

class content_generator_tokencode implements content_generator_interface {

    private static $instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance)
            self::$instance = new self ();

        return self::$instance;
    }

    public function generate($content_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($content_data)));

        $config = loader_config::getInstance()->getConfig('content')->urlDownloadContent;
        $content_data->url = str_replace(array('@TOKEN@', '@CODE@'), array($content_data->token, $content_data->code), $config);

        return $content_data;
    }

}