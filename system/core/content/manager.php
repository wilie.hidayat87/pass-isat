<?php

class content_manager {

    private static $instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance)
            self::$instance = new self ();

        return self::$instance;
    }

    public function getContent($content_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        /** dummy data
          $data = loader_data::get ( 'content' );
          $data->code = 'ABCDE12345';
          $data->name = 'Judul Content';
          $data->type = 'Game';
          $data->url = 'http://127.0.0.1/content_data/judul-content';

          return $data;
         * */
        $class_name = "content_generator_" . $content_data->generatorType;
        $class_name_default = "content_generator_default";

        if (class_exists($class_name)) {
            $content_generator = $class_name::getInstance();
        } else if (class_exists($class_name_default)) {
            $log->write(array('level' => 'info', 'message' => " Class Doesn't Exist : " . $class_name));
            $content_generator = $class_name_default::getInstance();
        } else {
            $log->write(array('level' => 'error', 'message' => " Class Doesn't Exist : " . $class_name . " & " . $class_name_default));
            return false;
        }

        return $content_generator->generate($content_data);
    }

    public function getBroadcastContent($broadcast_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $main_config = loader_config::getInstance()->getConfig('main');
        $operator_name = $main_config->operator;

        if ($broadcast_data->contentSelect == 'custom') {
            $class_name = $operator_name . '_content_' . strtolower($broadcast_data->contentLabel);
            $class_name_default = 'content_' . strtolower($broadcast_data->contentLabel);
        } else {
            $class_name = $operator_name . '_content_' . strtolower($broadcast_data->contentSelect);
            $class_name_default = 'content_' . strtolower($broadcast_data->contentSelect); //datepublish
        }

        //$class_name = $operator_name . '_content_' . strtolower ( $broadcast_data->contentSelect );
        //$class_name_default = 'content_' . strtolower ( $broadcast_data->contentSelect ); //datepublish


        if (class_exists($class_name)) {
            $content_generator = $class_name::getInstance();
        } else if (class_exists($class_name_default)) {
            $log->write(array('level' => 'info', 'message' => " Class Doesn't Exist : " . $class_name));
            $content_generator = $class_name_default::getInstance();
        } else if (class_exists("content_default")) {
            $log->write(array('level' => 'info', 'message' => " Class Doesn't Exist : " . $class_name . " & " . $class_name_default));
            $content_generator = content_default::getInstance();
        } else {
            $log->write(array('level' => 'error', 'message' => " Class Doesn't Exist : " . $class_name . " & " . $class_name_default . " & " . "content_default"));
            return false;
        }

        return $content_generator->get($broadcast_data);
    }

}

