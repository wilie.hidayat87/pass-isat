<?php
class quiz_owner_data{
	public $id;
	public $code;
	public $name;
	public $username;
	public $password;
	public $description;
	public $defaultMultiplier;
	public $maximumQuestionAllowed;
	public $maximumAnswerAllowed;
	public $hasSpecialDay;//boolean
	private $retryWrongAnswer;//boolean
}