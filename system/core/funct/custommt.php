<?php 

class funct_custommt {

	public function getDataFile($file,$sep='|') {
		$result = array();
		$records=file($file);
		foreach($records as $row) {
			$rowArr = explode($sep,$row);
			if(count($rowArr) > 1) {
				$result[$rowArr[0]] = trim($rowArr[1]);
			}
		}
		return $result;	
	}

	public function getCountPush($records,$msisdn) {
		$result = false;
		if(isset($records[$msisdn])) {
			$result = $records[$msisdn];
		}
		return $result;
	}

	public function isCount($count,$max) {
		$result = false;
		if($count == $max){
			$result = true;
		}
		return $result;
	}

	public function getLatestFile($path) {
		//$path = "/path/to/my/dir"; 

		$latest_ctime = 0;
		$latest_filename = '';    

		$d = dir($path);
		while (false !== ($entry = $d->read())) {
			$filepath = "{$path}/{$entry}";
			// could do also other checks than just checking whether the entry is a file
			if (is_file($filepath) && filectime($filepath) > $latest_ctime) {
				$latest_ctime = filectime($filepath);
				$latest_filename = $entry;
			}
		}
		return $latest_filename;
	}
	
}

?>
