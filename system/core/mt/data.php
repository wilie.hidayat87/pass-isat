<?php
class mt_data {
	
	public $inReply;
	public $msgId;
	public $msgIdXL;
	public $adn;
	public $msgData;
	public $msgLastStatus;
	public $retry;
	public $msgStatus;
	public $closeReason;
	public $price;
	public $operatorId;
	public $media;
	public $channel;
	public $service;
	public $partner;
	public $subject;
	public $keyword;
	public $operatorName;
	public $requestType;
	public $incomingDate;
	public $serviceId;
	public $msisdn;
	public $type;
	public $mo; // object mo_data
	public $charging; // object charging_data
	public $content_data; // object content_data
        public $pushBufferId;
}