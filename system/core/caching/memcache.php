<?php

class caching_memcache implements caching_interface {

    private $memcacheObj = array();
    private static $instance;

    private function __construct($profile) {
        $this->load($profile);
    }

    public static function getInstance($profile) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($profile)));

        if (!self::$instance) {
            self::$instance = new self($profile);
        }

        return self::$instance;
    }

    public function load($profile) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($profile)));

        $config_memcached = loader_config::getInstance()->getConfig('cache')->profile [$profile];
        $host = $config_memcached['host'];
        $port = $config_memcached['port'];
        if (!array_key_exists($profile, $this->memcacheObj) && (class_exists('Memcached'))) {
            $memcached = new Memcached ();
            $log->write(array('level' => 'debug', 'message' => "Add memcached server " . $host . ":" . $port));
            $memcached->addServer($host, $port);
            $this->memcacheObj [$profile] = $memcached;
        } else {
            $log->write(array('level' => 'info', 'message' => "Class Doesn't Exist : Memcached"));
            return false;
        }
    }

    public function get($caching_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($caching_data)));

        $profile = $caching_data->profile;
        if (class_exists('Memcached')) {
            $memcacheObj = $this->memcacheObj [$profile];
            $caching_data->value = $memcacheObj->get($caching_data->key);
        } else {
            $log->write(array('level' => 'info', 'message' => "Class Doesn't Exist : Memcached"));
            return false;
        }
        return $caching_data;
    }

    public function set($caching_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($caching_data)));

        $profile = $caching_data->profile;
        if (class_exists('Memcached')) {
            $memcacheObj = $this->memcacheObj [$profile];
            return $memcacheObj->set($caching_data->key, $caching_data->value, $caching_data->expire);
        } else {
            $log->write(array('level' => 'info', 'message' => "Class Doesn't Exist : Memcached"));
            return false;
        }
    }

}