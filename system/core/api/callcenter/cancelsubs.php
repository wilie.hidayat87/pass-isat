<?php

class api_callcenter_cancelsubs {

    public function process($GET) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($GET)));

        $check = $this->checkParams($GET);

        if ($check !== true) {
            return $check;
        }

        $user_data = new user_data ();
        $user_data->msisdn = $GET ['phone'];
        $user_data->adn = $GET ['largeAccount'];
        $user_data->transaction_id = $GET ['id'];
        $user_data->active = 1;

        $model = loader_model::getInstance();
        $model_user = $model->load('user', 'connDatabase1');
        $model_operator = $model->load('operator', 'connDatabase1');
        $main_config = loader_config::getInstance()->getConfig('main');

        $get_data = $model_user->get($user_data);
        if ($get_data === false) {
            return 'NOT_FOUND';
        }

        $user_data->operator_id = $model_operator->getOperatorId($main_config->operator);

        $mo_data = array('msisdn' => $GET ['phone'], 'shortnum' => $get_data ['adn'], 'msgtext' => loader_config::getInstance()->getConfig('spring')->unsubscribeKeyword . " " . $GET ['channel'], 'msgid' => date("YmdHis") . str_replace('.', '', microtime(true)), 'operator' => $user_data->operator_id, 'refmsgid' => $GET ['refmsgid']);

        $save_file = new manager_mo_processor ();
        $result = $save_file->saveToFile($mo_data);

        if ($result == 'OK') {
            $log->write(array('level' => 'debug', 'message' => "End : OK"));
            return 'OK';
        } else {
            $log->write(array('level' => 'debug', 'message' => "End : SYSTEM ERROR"));
            return 'ERROR';
        }
    }

    protected function checkParams($GET) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($GET)));

        if (empty($GET ['phone']) || empty($GET ['id']) || empty($GET ['channel'])) {
            $log->write(array('level' => 'debug', 'message' => "End : MISSING_PARAMETERS"));
            return 'MISSING_PARAMETERS';
        } elseif (!is_numeric($GET ['phone']) or (!empty($GET ['largeAccount']) && !is_numeric($GET ['largeAccount'])) or (!in_array($GET ['channel'], array("CALLCENTER", "VOX")))) {
            $log->write(array('level' => 'debug', 'message' => "End : INVALID_PARAMETERS"));
            return 'INVALID_PARAMETERS';
        } else {
            $log->write(array('level' => 'debug', 'message' => "End : OK"));
            return true;
        }
    }

}