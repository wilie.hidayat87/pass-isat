<?php

class api_callcenter_history {

    public function process($GET) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($GET)));

        $check = $this->checkParams($GET);

        if ($check != 'OK') {
            $log->write(array('level' => 'debug', 'message' => "End : " . $check));
            return $check;
        }

        $user_data = new user_data ();
        $user_data->msisdn = $GET ['phone'];
        if (!empty($GET ['largeAccount']))
            $user_data->adn = $GET ['largeAccount'];
        if (!empty($GET ['start']))
            $user_data->subscribed_from = $GET ['start'];
        if (!empty($GET ['end']))
            $user_data->subscribed_until = $GET ['end'];
        $user_data->active = 1;
        if (!empty($GET ['limit']))
            $user_data->limit = $GET ['limit'];

        $model_user = loader_model::getInstance()->load('user', 'connDatabase1');
        $spring_user = $model_user->getSpringDownloadHistory($user_data);

        if ($spring_user !== false and count($spring_user) > 0) {
            $xml = $this->createXML($spring_user);
            $log->write(array('level' => 'debug', 'message' => "End : " . $xml));
            return $xml;
        } else {
            $log->write(array('level' => 'debug', 'message' => "End : NOT_FOUND"));
            return 'NOT_FOUND';
        }
    }

    protected function checkParams($GET) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($GET)));

        if (empty($GET ['phone']) && empty($GET ['start']) && empty($GET ['end'])) {
            $log->write(array('level' => 'debug', 'message' => "End : MISSING_PARAMETERS"));
            return 'MISSING_PARAMETERS';
        } else if (!is_numeric($GET ['phone']) || (!empty($GET ['largeAccount']) && !is_numeric($GET ['largeAccount'])) || (!empty($GET ['limit']) && !is_numeric($GET ['limit']))) {
            $log->write(array('level' => 'debug', 'message' => "End : INVALID_PARAMETERS"));
            return 'INVALID_PARAMETERS';
        } else if (!empty($GET ['start'])) {
            if (!$this->is_valid_date($GET ['start'])) {
                $log->write(array('level' => 'debug', 'message' => "End : INVALID_PARAMETERS"));
                return 'INVALID_PARAMETERS';
            }
        } else if (!empty($GET ['end'])) {
            if (!$this->is_valid_date($GET ['end'])) {
                $log->write(array('level' => 'debug', 'message' => "End : INVALID_PARAMETERS"));
                return 'INVALID_PARAMETERS';
            }
        } else if (!empty($GET ['tariffType'])) {
            if (!$this->is_valid_tariff($GET ['tariffType'])) {
                $log->write(array('level' => 'debug', 'message' => "End : INVALID_PARAMETERS"));
                return 'INVALID_PARAMETERS';
            }
        }
        $log->write(array('level' => 'debug', 'message' => "End : OK"));
        return 'OK';
    }

    protected function createXML($users) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($users)));

        $config_spring = loader_config::getInstance()->getConfig('spring');
        $date = date("Y-m-d") . "T" . date("H:i:sO");
        $size = count($users);

        $xmlWriter = new library_xml_writer ();
        $xmlWriter->push('purchases');

        $xmlWriter->element('datetime-request', $date);
        $xmlWriter->element('size', $size);

        foreach ($users as $user) {
            #set channel
            if (!empty($user ['channel_subscribe']))
                $channel = $user ['channel_subscribe'];
            else
                $channel = $user ['channel_unsubscribe'];

            # set status and active
            if (is_numeric(trim($user ['status_charging']))) {
                $status = 'DELIVERED';
            } else {
                $status = trim($user ['status_charging']);
            }

            $xmlWriter->push('purchase');
            $xmlWriter->element('id', $user ['id']);
            $xmlWriter->element('datetime-purchase', $this->set_date($user ['purchase_time']));
            $xmlWriter->element('datetime-retry', '');
            $xmlWriter->element('portal-id', $config_spring->portalId);
            $xmlWriter->element('portal-name', $config_spring->portalName);
            $xmlWriter->element('large-account', $user ['adn']);
            $xmlWriter->element('content-type', '');
            $xmlWriter->element('protocol-number', $user ['protocol']);
            $xmlWriter->element('datetime-mt', $this->set_date($user ['mt_time']));
            $xmlWriter->element('datetime-mo', $this->set_date($user ['mo_time']));
            $xmlWriter->element('message-mo', htmlentities($user ['msg_data'], ENT_IGNORE, "UTF-8"));
            $xmlWriter->element('content-id', $user ['contentId']);
            $xmlWriter->element('content-name', $user ['contentName']);
            $xmlWriter->element('tariff-value', $user ['tariff']);
            $xmlWriter->element('tariff-type-id', $user ['tariffId']);
            $xmlWriter->element('tariff-type', $user ['Code']);
            $xmlWriter->element('credit', ($user ['tariff']?$user ['tariff']:0));
            $xmlWriter->element('channel', strtoupper($channel));
            $xmlWriter->element('status', $status);
            $xmlWriter->pop();
        }
        $xmlWriter->pop();

        $getXml = trim($xmlWriter->getXml());
        $log->write(array('level' => 'debug', 'message' => "End : " . $getXml));
        return $getXml;
    }

    private function set_date($date) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $date));

        if ($date) {
            $date = explode(" ", $date);
            $setDate = $date [0] . "T" . $date [1] . date("O");
        } else {
            $setDate = "";
        }

        $log->write(array('level' => 'debug', 'message' => "End : " . $setDate));
        return $setDate;
    }

    private function is_valid_date($value, $format = 'yyyy-mm-dd') {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $value . " with format : " . $format));

        if (strlen($value) >= 6 && strlen($format) == 10) {
            // find separator. Remove all other characters from $format
            $separator_only = str_replace(array('m', 'd', 'y'), '', $format);
            $separator = $separator_only [0]; // separator is first character


            if ($separator && strlen($separator_only) == 2) {
                // make regex
                $regexp = str_replace('mm', '(0?[1-9]|1[0-2])', $format);
                $regexp = str_replace('dd', '(0?[1-9]|[1-2][0-9]|3[0-1])', $regexp);
                $regexp = str_replace('yyyy', '(19|20)?[0-9][0-9]', $regexp);
                $regexp = str_replace($separator, "\\" . $separator, $regexp);

                //if ($regexp != $value && preg_match ( '/' . $regexp . '\z/', $value )) {
                // check date
                $arr = explode($separator, $value);
                $day = $arr [2];
                $month = $arr [1];
                $year = $arr [0];
                if (@checkdate($month, $day, $year))
                    return true;
                //}
            }
        }
        return false;
    }

    private function is_valid_tariff($type) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $type));

        $config_spring = loader_config::getInstance()->getConfig('spring');
        if (array_key_exists($type, $config_spring->protocol_type)) {
            return true;
        } else {
            return false;
        }
    }

}
