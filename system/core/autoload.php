<?php
/**
 * class autoload
 * make auto load file base on class name
 * @author Ardy Satria
 */

class autoload {
	
	public static $instance;
	
	protected $path = ''; // real path 
	protected $ext = '.php'; // file's extension
	

	/*
	 * constructor to set path and file's extension then register spl_autoload
	 * @param [optional] file's path
	 * @param [optional] file's extension
	 * @return void
	 */
	public function __construct($path = null, $ext = null) {                
		if (! empty ( $path ))
			$this->path = $path;
		else
			$this->path = realpath ( dirname ( __FILE__ ) );
		
		if (! empty ( $ext ))
			$this->ext = '.' . $ext;
		
		spl_autoload_register ( array ($this, 'load' ) );
		
	}
	
	/**
	 * singleton object
	 * @param [optional] $path
	 * @param [optional] $ext
	 * @return void
	 */
	public static function getInstance($path = null, $ext = null) {    
	            
		if (! self::$instance) {
			self::$instance = new self ( $path, $ext );
		}
		
		return self::$instance;
	}
	
	/**
	 * this function will include file based on class name 
	 * @param $class_name
	 * @return void
	 */
	public function load($class_name) {                
		//$dir0 = OPERATOR_PATH . '/default' . $this->get_file_dir ( $class_name );
		$dir1 = CORE_PATH . $this->get_file_dir ( $class_name );
		$dir2 = APP_PATH . $this->get_file_dir ( $class_name );
		$dir3 = CONFIG_PATH . $this->get_file_dir ( $class_name );
		$dir4 = OPERATOR_PATH . $this->get_file_dir ( $class_name );
		//echo "\n<br> class name : ".$class_name. "  ". $dir1 . ' - ' . $dir2 . ' - ' . $dir3 . ' - ' . $dir4 . "<br>";	
                if (is_readable ( $dir1 ) and file_exists ( $dir1 )) {
			require_once $dir1;
		} elseif (is_readable ( $dir2 ) and file_exists ( $dir2 )) {
			require_once $dir2;
		} elseif (is_readable ( $dir3 ) and file_exists ( $dir3 )) {
			require_once $dir3;
		} elseif (is_readable ( $dir4) and file_exists ( $dir4 )) {
			//var_dump(is_readable ( $dir0 ) , file_exists ( $dir0 ));
			//echo "\n<br>" . $dir0 . ' - ' . $dir4 . "<br>";	
			//if (is_readable ( $dir0 ) and file_exists ( $dir0 )) require_once $dir0;
                        require_once $dir4;
                } else {
			$this->set_error ( "ERROR : Class File Not Found or Not Readable :: $class_name" );
		}
	}
	
	/*
	 * @param class name
	 * @return string realdir name
	 */
	protected function get_file_dir($class_name) {                
		$tmp = explode ( "_", $class_name );
		$jlh = count ( $tmp );
		if ($jlh == 1) {
			return '/' . $class_name . $this->ext;
		} else if ($jlh > 1) {
			return '/' . implode ( '/', $tmp ) . $this->ext;
		} else {
			return;
		}
	}
	
	/**
	 * this function will handle error messages
	 * @param $message
	 * @return void
	 */
	public function set_error($message) {                
		//echo '<h1>' . $message . '</h1>';
	}

}
