<?php

class buffer_file {

    private static $instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance)
            self::$instance = new self ();

        return self::$instance;
    }

    /**
     * @param String $mo_mt = mo/mt
     */
    public function generate_file_name($content, $config_name = 'mo', $mo_mt = 'mo') {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $config_operator = loader_config::getInstance()->getConfig($config_name);

        $bufferPath = $config_operator->bufferPath;
        $bufferSlot = $config_operator->bufferSlot;
        $lastNum = (int) substr($content->msisdn, - 1);
		
        $path = $bufferPath . '/' . ($lastNum % $bufferSlot) . '/';
        if (!is_dir($path)) {
            $log->write(array('level' => 'debug', 'message' => "create a folder : " . $path));
            mkdir($path, 0777, TRUE);
        }

        return $path . uniqid() . '.' . strtoupper($mo_mt);
    }

    public function save($path, $content) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start path : " . $path . ". content :" . serialize($content)));

        $result = (boolean) file_put_contents($path, serialize($content));

        if (!$result && !is_writable($path)) {
            chmod($path, 0777);
        }

        if ($result === false) {
            $log->write(array('level' => 'error', 'message' => "Failed to write a file : " . $path . ". Content : " . serialize($content)));
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param String $mo_mt = mo/mt
     */
    public function read($path, $limit, $mo_mt = 'mo') {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start path : " . $path . ". limit : " . $limit . ". type : " . $mo_mt));

        if (is_dir($path)) {
            $dh = scandir($path);
            $n = 0;
            if ($dh != false) {
                foreach ($dh as $filename) {
                    if ($n == $limit) {
                        $log->write(array('level' => 'debug', 'message' => "Limit throttle : " . $limit));
                        break;
                    }
                    if (($filename != '.' or $filename != '..') and preg_match('/\.' . strtoupper($mo_mt) . '$/i', $filename)) {
                        $file = $path . '/' . $filename;
                        $content = file_get_contents($file);
                        if ($content !== false) {
                            $contents [] = array($file => unserialize(trim($content)));
                        } else {
                            $log->write(array('level' => 'error', 'message' => " Failed to write a file : " . $file));
                            return false;
                        }
                        $n++;
                    } else if (preg_match('/\.' . strtoupper($mo_mt) . '$/i', $filename)) {
                        $log->write(array('level' => 'info', 'message' => " Files extension is not registered  : " . $filename));
                    }
                }
                if ($n === 0)
                    return FALSE;
            } else {
                $log->write(array('level' => 'error', 'message' => " Files Is Empty or Not Found : " . $path));
                return FALSE;
            }
            return $contents;
        } else {
            $log->write(array('level' => 'error', 'message' => " Directory Doesn't Exist : " . $path));
            return FALSE;
        }
    }

    public function readString($path, $limit) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (is_dir($path)) {
            $dh = scandir($path);
            $n = 0;
            if ($dh != false) {
                foreach ($dh as $filename) {
                    if ($n == $limit) {
                        $log->write(array('level' => 'debug', 'message' => "Limit throttle : " . $limit));
                        break;
                    }
                    if (($filename != '.' and $filename != '..')) {
                        $file = $path . '/' . $filename;
                        $log->write(array('level' => 'debug', 'message' => " Write a file : " . $file));
                        $content = file_get_contents($file);
                        if ($content !== false) {
                            $contents [] = array($file => trim($content));
                        } else {
                            $log->write(array('level' => 'error', 'message' => " Failed to write a file : " . $file));
                            return false;
                        }
                        $n++;
                    }
                }
                if ($n === 0)
                    return FALSE;
            } else {
                $log->write(array('level' => 'error', 'message' => " Files Is Empty or Not Found : " . $path));
                return FALSE;
            }
            return $contents;
        } else {
            $log->write(array('level' => 'error', 'message' => " Directory Doesn't Exist : " . $path));
            return FALSE;
        }
    }

    public function delete($path) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (false === unlink($path)) {
            $log->write(array('level' => 'error', 'message' => " Cannot delete a file : " . $path));
            return false;
        } else {
            return true;
        }
    }
    
    public function saveString($path, $content) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start path : " . $path . ". content :" . serialize($content)));

        $result = (boolean) file_put_contents($path, $content);

        if (!$result && !is_writable($path)) {
            chmod($path, 0777);
        }

        if ($result === false) {
            $log->write(array('level' => 'error', 'message' => "Failed to write a file : " . $path . ". Content : " . serialize($content)));
            return false;
        } else {
            return true;
        }
    }

}
