<?php

class charging_manager {

    private static $instance;

    private function __construct() {

    }

    public static function getInstance() {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance)
            self::$instance = new self ();

        return self::$instance;
    }

    public function getCharging($data) {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $main_config = loader_config::getInstance ()->getConfig('main');
        $class_name = $main_config->operator . "_charging_manager";
        $class_name_default = "default_charging_manager";

        if (class_exists($class_name)) {
            $charging = $class_name::getInstance ();
        } elseif (class_exists($class_name_default)) {
            $log->write(array('level' => 'info', 'message' => "Class Doesn't Exist : " . $class_name));
            $charging = $class_name_default::getInstance ();
        } else {
            $log->write(array('level' => 'error', 'message' => "Class Doesn't Exist : " . $class_name . " & " . $class_name_default));
            return false;
        }

        $result = $charging->getCharging($data);
        if ($result !== false) {
            return $result;
        } else {
            return false;
        }
    }

}