<?php

class queue_activemq {

    private $obj = NULL;

    /**
     * 
     * @param array $profile
     * @return boolean
     * 
     * array (
     * 'type' => 'activemq', 'protocol' => 'tcp', 'server'	=> '127.0.0.1', 'port' => '6113',
     * 'prefix' => 'tselMT', 'slot' => '2', 'retry' => '5'
     * )
     * 
     * connect to active mq server with given profile
     * 
     */
    public function connect($profile) {
        //$log = manager_logging::getInstance();
        //$log->write(array('level' => 'debug', 'message' => "Start"));

        try {
            $queueAddress = sprintf('%s://%s:%s', $profile ['protocol'], $profile ['server'], $profile ['port']);
		   // $log->write(array('level' => 'info', 'message' => $queueAddress));
            // stomp install pcntl
            $this->obj = new Stomp($queueAddress);
            return TRUE;

            // stomp library
            // $this->obj = new library_Stomp($queueAddress);
            // $this->obj->connect();
            //$this->obj->setReadTimeout(0,70000);
        } catch (StompException $e) {
            //$log->write(array('level' => 'error', 'message' => $e->getMessage()));
            echo $e->getMessage();
            return FALSE;
        }
    }

    /**
     * 
     * @param char $queueName
     * @return boolean
     * 
     * subscribe to queue channel, use this before get any data
     * 
     */
    public function subscribe($queueName) {
        //$log = manager_logging::getInstance();
        //$log->write(array('level' => 'debug', 'message' => "Start : " . $queueName));

        try {
            $channel = $this->validateChannel($queueName);
            $this->obj->subscribe($channel);
            return TRUE;
        } catch (StompException $e) {
            //$log->write(array('level' => 'error', 'message' => $e->getMessage()));
            return FALSE;
        }
    }

    /**
     * 
     * @param queue_data $data
     * @return boolean
     * 
     * put data into queue
     * 
     */
    public function put(queue_data $data) {
    //public function put($data) {
        //$log = manager_logging::getInstance();
        //$log->write(array('level' => 'debug', 'message' => "Start : " . serialize($data)));

        try {
            $channel = $this->validateChannel($data->channel);
            $result = $this->obj->send($channel, $data->value, array('persistent' => 'true'));
          //  $log->write(array('level' => 'info', 'message' => "Activemq send : " . print_r($result, 1)));
            return $result;
        } catch (StompException $e) {
           // $log->write(array('level' => 'error', 'message' => $e->getMessage()));
            //echo $e->getMessage();
            return FALSE;
        }
    }

    /**
     * 
     * retrieve data from queue
     * 
     * @uses : run method subscribe first before this method
     * @return char, FALSE if empty
     * 
     */
    public function pop() {
        //$log = manager_logging::getInstance();
        //$log->write(array('level' => 'debug', 'message' => "Start"));

        try {
            $msg = $this->obj->readFrame();
            if ($msg != NULL) {
                $this->obj->ack($msg);
                return $msg;
            } else {
                //$log->write(array('level' => 'info', 'message' => "Data empty"));
                return FALSE;
            }
        } catch (StompException $e) {
            //$log->write(array('level' => 'error', 'message' => $e->getMessage()));
            return FALSE;
        }
    }

    /**
     * 
     * @param char $string channel name
     * @return string
     * 
     */
    private function validateChannel($string) {
    //    $log = manager_logging::getInstance();
     //   $log->write(array('level' => 'debug', 'message' => "Start : " . $string));

        if ($string == '') {
       //     $log->write(array('level' => 'error', 'message' => "Missing Parameter"));
            return FALSE;
        }
        $pos = strpos(trim($string), '/queue/');
        if ($pos !== 0)
            $string = '/queue/' . $string;
        return $string;
    }

    private function saveToFile($strData) {
       // $log = manager_logging::getInstance();
       // $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($strData)));

        try {
            $config_retry = loader_config::getInstance()->getConfig('retry');

            $buffer_file = buffer_file::getInstance();
            $filename = uniqid() . ".queue";
            $path = $config_retry->bufferPathQueue . "/" . $filename;
            return $buffer_file->saveString($path, $strData);
        } catch (StompException $e) {
         //   $log->write(array('level' => 'error', 'message' => $e->getMessage()));
            return FALSE;
        }
    }

}
