<?php
class tools_encrypt {
	public function encrypt($string, $key) {
                $log = manager_logging::getInstance ();
                $log->write ( array ('level' => 'debug', 'message' => "Start" ) );
                
		$result = "";
		for($i = 0; $i < strlen ( $string ); $i ++) {
			$char = substr ( $string, $i, 1 );
			$keychar = substr ( $key, ($i % strlen ( $key )) - 1, 1 );
			$char = chr ( ord ( $char ) + ord ( $keychar ) );
			$result .= $char;
		}
		return base64_encode ( $result );
	}
	
	public function decrypt($string, $key) {
                $log = manager_logging::getInstance ();
                $log->write ( array ('level' => 'debug', 'message' => "Start" ) );
                
		$string = base64_decode ( $string );
		for($i = 0; $i < strlen ( $string ); $i ++) {
			$char = substr ( $string, $i, 1 );
			$keychar = substr ( $key, ($i % strlen ( $key )) - 1, 1 );
			$char = chr ( ord ( $char ) - ord ( $keychar ) );
			$result .= $char;
		}
		return $result;
	}
}