<?php
class summarizer_default implements summarizer_interface {
	public function execute(summarizer_data $summarizer){
		$log = manager_logging::getInstance ();
        $log->write ( array ('level' => 'debug', 'message' => "Start" ) );
        
		$loader_model = loader_model::getInstance();
		
		$from_model = $loader_model->load("tblmsgtransact", $summarizer->from);
		$from_data = $from_model->getSummary($summarizer);
		
		$to_model = $loader_model->load("report", $summarizer->to);
		$to_data = $to_model->saveSummary($from_data, $summarizer);
		
		
		/*echo "---------------\$from_data--------------\n";
		print_r($from_data);
		echo "------------end of \$form_data----------\n";*/
		return true;
	}
}