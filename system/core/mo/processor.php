<?php
interface mo_processor {
	public function saveToFile($mo_data);
	public function process($slot);
}