<?php

class smartcharge_functions {
	
	public function process_on_dr($drData) {
		
		// check only for insufficient credit
		$loader_config = loader_config::getInstance();
		$config_sc = $loader_config->getConfig('smartcharge');

		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Check Smartcharge : ".serialize($drData)." -- ".serialize($config_sc->dr_filtered) ) );
		
		if (in_array($drData->extStatus, $config_sc->dr_filtered)) {
		//if (in_array($drData->msgStatus, $config_sc->dr_filtered)) {
			
			// load smartcharge data
			$smartcharge_data = new model_data_smartchargetransact();
			$smartcharge_data->msisdn = $drData->msisdn;
			$smartcharge_data->tid = $drData->msgId;
			$smartcharge_data->sid = $drData->serviceId;
			$smartcharge_data->next_sid = $drData->next_sid;
			$smartcharge_data->status = 1;
			
			// read smartcharge
			$smartcharge_mt = $this->read_smartcharge_mt($smartcharge_data);
			
			// checked max attempt, if false just leave it
			if ($smartcharge_mt['attempt'] < $config_sc->allowed_attempt) {
				// create and send new mt
				$next_mt_data = $this->next_smartcharge_mt($smartcharge_mt);
				$this->send_next_mt($next_mt_data);
				
				// update attempt
				$this->update_smartcharge_mt($smartcharge_mt['id'], $smartcharge_mt['attempt'] + 1);
			}
			
		}
		
	}

	public function send_next_mt($mt) {
		
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Smartcharge Next MT." ) );
		
		$loader_config = loader_config::getInstance ();
		$configMT = $loader_config->getConfig ( 'mt' );
		$profile = $mt->charging->senderType;
		
		$param = 'username=yatta';
		$param .= '&password=esemesyatta';
		$param .= '&smsc=XLMTnew';
		$param .= '&from=' . $mt->charging->chargingId;
		$param .= '&to=' . $mt->msisdn;
		$param .= '&text=' . urlencode ( $mt->msgData );
		
		$metadata = '';
		if ($mt->shortname)
			$metadata .= 'Shortname=' . urlencode ( $mt->shortname ) . '&';
		
		//if ($mt->msgId) $metadata .= 'TX_ID='.urlencode($this->hex2bin($mt->msgId));
		if ($mt->msgId && strpos ( strtoupper ( $mt->subject ), 'MT;PULL;' ) !== FALSE)
			$metadata .= 'TX_ID=' . urlencode ( $this->hex2bin ( $mt->msgId ) );
		if ($metadata)
			$param .= '&meta-data=' . urlencode ( '?smpp?' . $metadata );
		
		$param .= '&dlr-mask=' . $configMT->profile [$profile] ['dr'] ['dlr-mask'];
		$param .= '&dlr-url=' . urlencode ( $configMT->profile [$profile] ['dr'] ['dlr-url'] . '?txid=' . $mt->msgId . '&status=%d&answer=%A&ccode=%P&msisdn=%p&ts=%t&adn=' . $mt->adn );
		//if ($smartcharge) $param .= urlencode('&sc=1&nc='.$smartcharge_info['next_sid']);
		
		$url = $configMT->profile [$profile] ['sendUrl'] [0];
		
		$hit = http_request::get ( $url, $param, $configMT->profile [$profile] ['SendTimeOut'] ); // uncomment on production
		$log->write ( array ('level' => 'debug', 'message' => "Kannel Url:" . $url . '?' . $param . ', Result:' . $hit ) );
		
		$response = explode ( ':', trim ( $hit ) );
		
		$mt->msgLastStatus = 'DELIVERED';
		$configDr = loader_config::getInstance ()->getConfig ( 'dr' );
		if ($configDr->synchrounous === TRUE) {
			if ($response [0] == '1') {
				$mt->msgStatus = 'DELIVERED';
			} else {
				$mt->msgStatus = 'FAILED';
			}
			$mt->closeReason = $hit;
		} else {
			if ($response [0] != '1') {
				$mt->closeReason = $hit;
			}
		}
		
		//var_dump($url, $param, $saveMT);		
		return loader_model::getInstance()->load('tblmsgtransact', 'connDatabase1')->saveMT($mt);
		
	}	
	
	public function next_smartcharge_mt($smartcharge_mt) {
		
		$prev_mt_data = unserialize($smartcharge_mt['data']);
		$next_mt_data = unserialize($smartcharge_mt['data']);
		$next_mt_data->serviceId = $smartcharge_mt['next_sid'];
		$next_mt_data->subject = $smartcharge_mt['subject'];
		
		$next_charging = $this->get_next_smartcharging($next_mt_data);

		$next_mt_data->price = $next_charging['gross'];
		$next_mt_data->charging->id = $next_charging['id'];
		$next_mt_data->charging->operator = $next_charging['operator'];
		$next_mt_data->charging->adn = $next_charging['adn'];;
		$next_mt_data->charging->chargingId = $next_charging['charging_id'];
		$next_mt_data->charging->gross = $next_charging['gross'];
		$next_mt_data->charging->netto = $next_charging['netto'];
		$next_mt_data->charging->username = $next_charging['username'];
		$next_mt_data->charging->password = $next_charging['password'];
		$next_mt_data->charging->senderType  = $next_charging['sender_type'];
		$next_mt_data->charging->messageType = $next_charging['message_type'];
		$next_mt_data->mo->msgId = $smartcharge_mt['tid'];
		$next_mt_data->msgId = $smartcharge_mt['tid'];

		//var_dump($prev_mt_data, '------- delimiter ------------', $next_mt_data);
		
		return $next_mt_data;
	}
	
	public function update_smartcharge_mt($id, $numberofattempt) {
		$model_smartcharge = loader_model::getInstance ()->load ( 'smartcharge', 'connDatabase1' );
		$result = $model_smartcharge->update_smartcharging_mt($id, $numberofattempt);
		return $result;
	}
	
	public function get_next_smartcharging($mt) {
		$model_smartcharge = loader_model::getInstance ()->load ( 'smartcharge', 'connDatabase1' );
		$next_smartcharge = $model_smartcharge->get_next_smartcharging ( $mt );
		return $next_smartcharge;
	} 
	
	public function isSmartCharge($mt) {
		$model_smartcharge = loader_model::getInstance ()->load ( 'smartcharge', 'connDatabase1' );
		$isSmartCharge = $model_smartcharge->isSmartCharge ( $mt );
		return $isSmartCharge;
	}

	public function save_smartcharge_mt($data) {
		$model_smartcharge = loader_model::getInstance ()->load ( 'smartcharge', 'connDatabase1' );
		$result = $model_smartcharge->save_smartcharge_mt($data);
		return $result;
	}
	
	public function read_smartcharge_mt($data) {
		$model_smartcharge = loader_model::getInstance ()->load ( 'smartcharge', 'connDatabase1' );
		$result = $model_smartcharge->read_smartcharge_mt($data);
		return $result;
	}

	private function hex2bin($str) {
		$bin = "";
		$i = 0;
		do {
			$bin .= chr ( hexdec ( $str {$i} . $str {($i + 1)} ) );
			$i += 2;
		} while ( $i < strlen ( $str ) );
		return $bin;
	}
}

?>
