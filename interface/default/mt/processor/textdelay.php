<?php

#class default_mt_processor_textdelay implements default_mt_processor_text {a
class default_mt_processor_textdelay {

    private static $instance;

    private function __construct() {

    }

    public static function getInstance() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance) {
            self::$instance = new self ();
        }
        return self::$instance;
    }

    public function saveToQueue($mt_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start Save MT to Buffer Delay: " . serialize($mt_data)));

        $mt_delay = new mt_delay_data ();
        $mt_delay->service = $mt_data->service;
        $mt_delay->adn = $mt_data->adn;
        $mt_delay->msisdn = $mt_data->msisdn;
        $mt_delay->obj = serialize($mt_data);

        $model_mtdelay = loader_model::getInstance()->load('mtdelay', 'connDatabase1');
        $model_mtdelay->add($mt_delay);

        return true;
    }

}
