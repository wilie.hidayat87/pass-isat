<?php

class config_database {

    public $profile = array(
        'connDatabase1' =>
        array(
            'username' => 'xmp',
            'password' => '123456',
            'host' => '127.0.0.1',
            'database' => 'xmp',
            'driver' => 'mysql'
        ),
         'connDatabase2' =>
        array(
            'username' => 'root',
            'password' => '',
            'host' => '127.0.0.1',
            'database' => 'xmp_bak',
            'driver' => 'mysql'
        ),
        'connBroadcast' =>
        array(
            'username' => 'dbpush',
            'password' => '123456',
            'host' => '127.0.0.1',
            'database' => 'dbpush',
            'driver' => 'mysql'
        ),
        'reports' =>
        array(
            'username' => 'reports',
            'password' => '123456',
            'host' => '127.0.0.1',
            'database' => 'reports',
            'driver' => 'mysql'
        ),
        'connWap' =>
        array(
            'username' => 'wap',
            'password' => '123456',
            'host' => '127.0.0.1',
            'database' => 'wap',
            'driver' => 'mysql'
        ),
		 'connDr' =>
        array(
            'username' => 'cdr',
            'password' => '123456',
            'host' => '127.0.0.1',
            'database' => 'cdr',
            'driver' => 'mysql'
        ),
        'connHadoop' =>
        array(
            'driver' => 'hadoop'
        )
    );

}
