<?php

final class default_service_module_notification implements service_module_interface {

    public function run(mo_data $mo, service_reply_data $reply) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $loader_model = loader_model::getInstance();

        /* @var $replyAttribute model_replyattribute */
        $log->write(array('level' => 'debug', 'message' => "GET Atribute"));

        # load reply attributes value
        $replyAttributes = array();
        $replyAttributeModel = $loader_model->load('replyattribute', 'connDatabase1');
        $attributes = $replyAttributeModel->get($reply->id);
        foreach ($attributes as $attribute) {
            $replyAttributes [$attribute ['name']] = $attribute ['value'];
        }

        /* @var $user_manager user_manager */
        $user_manager = user_manager::getInstance();
        $mt_processor = new manager_mt_processor ( );

        # save MO
        $mo->inReply = $mt_processor->saveMOToTransact($mo);
        $mt = $this->getMsg($mo, $reply, $replyAttributes);
		
        return $mo;
    }

    private function getMsg($mo, $reply, $replyAttributes) {

        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $mt_manager = new manager_mt_processor ( );
        //$reply_id = $mt_manager->saveMOToTransact ( $mo );

        $mt = loader_data::get('mt');

        $mt->msgData = $reply->message;
        $mt->msgId = $mo->msgId;
        $mt->charging = $reply->chargingId;
        $mt->adn = $mo->adn;
        $mt->channel = $mo->channel;
        $mt->msisdn = $mo->msisdn;
        $mt->operatorId = $mo->operatorId;
        $mt->inReply = $mo->inReply;
        $mt->price = $reply->price;
        $mt->operatorName = $mo->operatorName;
        $mt->service = $mo->service;
        $mt->subject = strtoupper('MT;PULL;' . $mo->channel . ';TEXT');
        $mt->type = 'mtack';
        $mt->mo = $mo;

        $mt_manager->saveToQueue($mt);

        return $mo;
    }
}

?>
