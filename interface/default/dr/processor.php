<?php

class default_dr_processor implements dr_processor_interface {

    public function process($slot) {
    	//echo($slot.'</br>');
        $log_profile = 'dr_processor';
	$log = manager_logging::getInstance();
	$log->setProfile($log_profile);
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . $slot));

        $load_config = loader_config::getInstance();
        $buffer_file = buffer_file::getInstance();

        $config_dr = $load_config->getConfig('dr');

        $path = $config_dr->bufferPath . '/' . $slot;
        $limit = $config_dr->bufferThrottle;
        $result = $buffer_file->read($path, $limit, 'dr');

        if ($result !== false) {
            foreach ($result as $val) {
                foreach ($val as $drDataPath => $content) {
                    $log->write(array('level' => 'debug', 'message' => "path : " . $drDataPath . " content : " . serialize($content)));
                    
                    $log->writeDefault("dr_process",$content);

                    if (is_object($content)) {
                    	//print_r($content);
                        $drSave = $this->saveToDb($content);
                        if ($drSave) {
                            $buffer_file->delete($drDataPath);
                        }
                    } else {
                        $log->write(array('level' => 'error', 'message' => "buffer DR is not an object"));
                        $buffer_file->delete($drDataPath);
                    }
                }
            }
        }
    }

    public function saveToDb($str) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($str)));

        $drData = $this->getDRData($str);

        $load_config = loader_config::getInstance();
        $config_dr = $load_config->getConfig('dr');
	//$storageJojoku = loader_model::getInstance()->load('charging', 'connDatabase3');

        $type = 'text';
        if ($config_dr->responseMap [$type] [$str->msgStatus]) {
            $str->closeReason = $config_dr->responseMap [$type] [$str->msgStatus];
        } else {
            $str->closeReason = "UNKNOWN";
        }
		
        //echo $str->closeReason;
        
        //$drData->statusText = $str->msgData;
        $drData->statusCode = $str->msgStatus;
        $drData->statusInternal = $str->closeReason;
        $drData->cdrHour = date('G');

        $this->saveToFile($drData);
        $save = loader_model::getInstance()->load('cdr', 'connDr')->create($drData);
	error_log(date('Ymd').print_r($drData,1)."\n",3,'/tmp/proxl_dr_delivered.log');

/*
	if(isset($drData->closeReason) && $drData->closeReason=='DELIVERED') {
                error_log(date('Ymd').print_r($drData,1)."\n",3,'/tmp/proxl_dr_delivered.log');
                if(isset($drData->service) && $drData->service=='heboh2_register') {
                        error_log(date('Ymd').print_r($drData,1)."\n",3,'/tmp/proxl_dr_heboh.log');
			$sub_detail = loader_model::getInstance()->load('subsdetail', 'connDatabase1');
			$data = new model_data_subsdetail();
			$data->id_subscription = 0;
			$data->msisdn = $drData->msisdn;
			$data->desc = 'heboh2_register';

			if($sub_detail->isNew($data)) {
			        $sub_detail->add($data);
			} else {
			        $sub_detail->inc_counter($data);
			}
                }
        }
*/
        $log->write(array('level' => 'debug', 'message' => 'Return Value for Save is ' . $save));

/*
	$data['msisdn'] = $str->msisdn;
	$fetchJojoku = $storageJojoku->getJojokuStorage($data);

	if(!empty($fetchJojoku))
	{
		if($str->status == "2" && strpos($fetchJojoku['keyword'], "jojoku") !== false)
		{
			$url = "http://182.16.255.14/jojo/index.php/web_service/add_point_from_xmp/".$fetchJojoku['msisdn']."/success/".date("YmdHis")."/".$fetchJojoku['charging']."";
			$log->write(array('level' => 'debug', 'message' => 'Hit URL JOJOKU : ' . $url));

			$hit = http_request::get($url, "", "30");

			$data['status'] = $str->status;
			$storageJojoku->updateJojokuStorage($data);
			$log->write(array('level' => 'debug', 'message' => 'Status DB updated.'));
		}
	}
*/
        return true;
    }

    /**
     * update from cdr table to transact
     *
     * @param 
     * 		array
     * 			-q : from hour
     * 			-w : to hour
     * 			-c : conn DB used to updating
     * 			-f : from database.table
     * 			-t : to database.table
     * 			
     */
    public function updateTransact($arr) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($arr)));

        if (empty($arr['c'])) {
            $log->write(array('level' => 'debug', 'message' => 'Parameter missing, exiting script'));
            return false;
        }

        $configDr = loader_config::getInstance()->getConfig('dr');
        //edit tambah newxmp di depan tbl_msgtransact : Yudha
        $parameter = array(
            'q' => date('H'),
            'w' => date('H', strtotime('-' . $configDr->defaultHour . ' hour')),
            'f' => 'cdr.cdr_' . date('Ymd'),
            't' => 'newxmp.tbl_msgtransact'
        );

        foreach ($parameter as $params => $value) {
            if (!isset($arr[$params])) {
                $arr[$params] = $value;
            }
        }
        return loader_model::getInstance()->load('cdr', $arr['c'])->updateTransact($arr);
    }

    public function saveToFile($str) {
    	//print_r($str);
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start'));

        $config_hadoop = loader_config::getInstance()->getConfig('hadoop');
        if ($config_hadoop->enableDR == true) {
            return loader_model::getInstance()->load('dr', 'connHadoop')->saveDR($str);
        } else {
            return false;
        }
    }

    public function saveToBuffer($str) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($str)));

        $config_dr = loader_config::getInstance()->getConfig('dr');

        $file_data = loader_data::get('file');
        $filename = date('Ymd') . str_replace('.', '', microtime(true)) . ".dr";
        $file_data->path = $config_dr->bufferPath . "/" . $filename;

        $file_data->content = serialize($str);
		
        //tambahan yudha=====================================================================
        $buffer_file = buffer_file::getInstance();
        
        $path = $buffer_file->generate_file_name($str,'dr','dr');
        
        //echo($path);
        
        $file_data->path=$path;
        //===================================================================================
        
        if (logging_file::writeDBFile($file_data)) {
            return "OK";
        } else {
            return "NOK";
        }
    }

    protected function getDRData($mt_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($mt_data)));

        $model_transact = loader_model::getInstance()->load('tblmsgtransact', 'connDatabase1');
        $dataTansact = $model_transact->getDRTransact($mt_data);

        $chargingData = loader_data::get('charging');
        $chargingData->chargingId = $dataTansact[0]['chargingId'];
        $chargingData->senderType = $dataTansact[0]['sender_type'];

        $drData = loader_data::get('dr');
        $drData->msgId = $dataTansact[0]['MSGINDEX'];
        $drData->operatorId = $dataTansact[0]['OPERATORID'];
        $drData->subject = $dataTansact[0]['SUBJECT'];
        $drData->charging = $chargingData;

        return $drData;
    }

}
