<?php

class indosat_service_module_delayingmt {

    public function delay($mo, $mt) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));
		
		$excludeService = array('VIDEO','JOJOKU_99790_ISAT');
		$excludeWelcomeMessageBySubkeyword = array('UMB');
		
		if(in_array(strtoupper($mo->service), $excludeService) && in_array(strtoupper($mo->customService), $excludeWelcomeMessageBySubkeyword)) 
		{
			$mt->isDelay = TRUE;
			$mt->delayTime = date("Y-m-d", strtotime("+1 day")) . " 04:00:00";
		}
		
        return $mt;
    }
}
