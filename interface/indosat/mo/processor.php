<?php

class indosat_mo_processor extends default_mo_processor {

    /**
     * @param $arrData
     */
    public function saveToFile($arrData) {
        $log = manager_logging::getInstance();
	$config_mo = loader_config::getInstance ()->getConfig('mo');
	//$storageJojoku = loader_model::getInstance()->load('charging', 'connDatabase3');

        $log->write(array('level' => 'debug', 'message' => "Start"));

        $log->write(array('level' => 'info', 'message' => "Info : " . print_r($arrData, TRUE)));

        $load_config = loader_config::getInstance();
        $config_main = $load_config->getConfig('main');
        $ip = $_SERVER ['REMOTE_ADDR'];

        $config_mo = $load_config->getConfig('mo');
        $mo_data = loader_data::get('mo');
		$mo_data->msisdn = $arrData ['msisdn'];
        $mo_data->msgId = $arrData ['transid'];
        $mo_data->rawSMS = $arrData ['sms'];
		
		$split_raw_sms = explode(" ", $mo_data->rawSMS);
		if(count($split_raw_sms) > 2){
			$trigger = $split_raw_sms[0]; // REG
			$service = $split_raw_sms[1]; // GOLD
			$cs = $split_raw_sms[2]; // LAST KEYWORD GV/VIC
			$ck = $split_raw_sms[3]; // CAMPAIGN KEYWORD
			
			$mo_data->customService = $cs;
		}
		
        $mo_data->adn = $arrData['sc'];
        $mo_data->operatorName = $config_main->operator;
        if (!empty($arrData ['trx_time']))
            $trx_date = $arrData ['trx_time'];
        else
            $trx_date = '';
        $mo_data->incomingDate = $this->setDate($trx_time);
        $mo_data->incomingIP = http_request::getRealIpAddr();
        $mo_data->substype = $arrData['substype'];
        $mo_data->type = 'mtpull';

        //var_dump($mo_data);
        /*
	if(strpos(strtolower($arrData ['sms']), "jojoku") !== false)
	{
		$sms = strtolower($arrData ['sms']);
		if($sms == $config_mo->jojokuStorage[$sms]['keyword']){
			$arrData['status'] = "0";
			$arrData['charging'] = $config_mo->jojokuStorage[$sms]['charging'];
			$arrData['charging_point'] = $config_mo->jojokuStorage[$sms]['charging_point'];
			$arrData['keyword'] = $config_mo->jojokuStorage[$sms]['keyword'];
		}
		$log->write(array('level' => 'info', 'message' => 'Start save jojoku : '));
		$storageJojoku->saveJojokuStorage($arrData);
	}
*/
        $buffer_file = buffer_file::getInstance();

        $path = $buffer_file->generate_file_name($mo_data);
        
        $save_file = $buffer_file->save($path, $mo_data);
        if ($save_file) {
            $log->write(array('level' => 'info', 'message' => 'Object MO write at: ' . $path . ' response : ' . $config_mo->returnCode['OK']));
            return $config_mo->returnCode ['OK'];
        } else {
            $log->write(array('level' => 'error', 'message' => 'Write Object MO failed at : ' . $path . ' response : ' . $config_mo->returnCode['OK']));
            return $config_mo->returnCode ['NOK'];
        }
    }

    private function setDate($char) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $log->write(array('level' => 'info', 'message' => "Info : " . $char));

        if (empty($char)) {
            return date("Y-m-d H:i:s");
        } else {
            $y = substr($char, 0, 4);
            $m = substr($char, 4, 2);
            $d = substr($char, 6, 2);
            $h = substr($char, 8, 2);
            $i = substr($char, 10, 2);
            $s = substr($char, 12, 2);
            $datetime = $y . '-' . $m . '-' . $d . ' ' . $h . ':' . $i . ':' . $s;
            return $datetime;
        }
    }
}
