<?php
class config_mo {
	//public $bufferPath = '/app/buffers/indosat';
	public $bufferPath = '/app/xmp2012/buffers/indosat';
	public $bufferSlot = 10;
	public $returnCode = array ('OK' => 'OK', 'NOK' => 'NOK' );
	public $bufferThrottle = 500;
	public $jojokuStorage = array('jojoku1' => array('keyword' => 'jojoku1', 'charging' => '1100', 'charging_point' => '50'), 
				      'jojoku2' => array('keyword' => 'jojoku2', 'charging' => '2200', 'charging_point' => '100'),
				      'jojoku3' => array('keyword' => 'jojoku3', 'charging' => '3200', 'charging_point' => '200'),
				      'jojoku5' => array('keyword' => 'jojoku5', 'charging' => '5500', 'charging_point' => '300'),
				      'jojoku10' => array('keyword' => 'jojoku10', 'charging' => '11000', 'charging_point' => '500'));
}
