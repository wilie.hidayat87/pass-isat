<?php

class indosat_api_status 
{
	public function process($data) 
	{
		$log = manager_logging::getInstance();
		$xmp = loader_model::getInstance()->load('tblmsgtransact', 'connDatabase1');

		$data['status'] = "1";
		$data['service'] = "JOJOKU_99790_ISAT";
		
		$result = $xmp->getUserActiveJojoku($data);
		if(count($result) > 0){
			return "OK";
		}else{
			return "NOK";
		}
	}
}