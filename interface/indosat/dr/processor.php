<?php

class indosat_dr_processor extends default_dr_processor {

    private function _setStatus($drData)
    {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($drData)));

        $config_main = loader_config::getInstance()->getConfig('main');
        //var_dump($drData);

        $mtData = loader_data::get('mt');
        $mtData->closeReason = $drData->statusCode;
        $mtData->msgId = $drData->msgId;

        $mtData->adn = $config_main->adn;
        //$mtData->adn = $drData->adn; get adn from config
        $mtData->msisdn = $drData->msisdn;
        $mtData->status = $drData->statusInternal;
        $mtData->serviceId = $drData->serviceId;

        return loader_model::getInstance()->load('tblmsgtransact', 'connDatabase1')->setStatus($mtData);
    }

    /**
     * update from cdr table to transact
     *
     * @param
     *          array
     *                  -q : from hour
     *                  -w : to hour
     *                  -c : conn DB used to updating
     *                  -f : from database.table
     *                  -t : to database.table
     *
     */
    public function updateTransact($arr) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($arr)));

        if (empty($arr['c'])) {
            $log->write(array('level' => 'debug', 'message' => 'Parameter missing, exiting script'));
            return false;
        }

        $configDr = loader_config::getInstance()->getConfig('dr');
        $parameter = array(
            'q' => date('H'),
            'w' => date('H', strtotime('-' . $configDr->defaultHour . ' hour')),
            'f' => 'cdr.cdr_' . date('Ymd'),
            't' => 'tbl_msgtransact'
        );

        foreach ($parameter as $params => $value) {
            if (!isset($arr[$params])) {
                $arr[$params] = $value;
            }
        }
        return loader_model::getInstance()->load('cdr', $arr['c'])->updateTransact($arr);
    }

    public function saveToDb($str) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($str)));

        $drData = $this->getDRData($str);

        $load_config = loader_config::getInstance();
        $config_dr = $load_config->getConfig('dr');
        $type = 'text';

        $drData->closeReason = $str->closeReason;
        $drData->statusText = $str->statusText;
        $drData->statusCode = $str->statusCode;
        $drData->statusInternal = $str->msgStatus;
        $drData->serviceId = $str->serviceId;
        $drData->cdrHour = date('G');
        $save = loader_model::getInstance()->load('cdr', 'connDr')->create($drData);
        $log->write(array('level' => 'debug', 'message' => 'Return Value for Save is ' . $save));

        //var_dump($drData);

        $this->_setStatus($drData);
		
		if(strtoupper($drData->service) == "JOJOKU_99790_ISAT")
		{
			if($this->filterMembership($drData)) $this->membership($drData);
		}

        return true;
    }

    public function saveToBuffer($str)
    {
        /**
         *  http://ip:port://?time=[]&serviceid=[]&tid=[]&dest=[]&status=[]
         **/

        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($str)));

        $config_main = loader_config::getInstance()->getConfig('main');

        $config_dr = loader_config::getInstance()->getConfig('dr');
        $dr_data = loader_data::get('dr');
        $buffer_file = buffer_file::getInstance();

        $dr_data->msisdn = (isset($_GET['dest'])) ? $_GET['dest'] : '';
        $dr_data->msgId = (isset($_GET['tid'])) ? $_GET['tid'] : '';
        $dr_data->adn = (isset($_GET['adn'])) ? $_GET['adn'] : $config_main->adn;
        $dr_data->dateCreated = (isset($_GET['time'])) ? $_GET['time'] : date('Y-m-d H:i:s');
        $dr_data->statusCode = (isset($_GET['status'])) ? $_GET['status'] : '';
        $dr_data->serviceId = (isset($_GET['serviceid'])) ? $_GET['serviceid'] : '';
        $dr_data->statusText = $config_dr->responseText[(int)$_GET['status']];

        if (isset($_GET['status'])) {
                        $dr_data->msgStatus = $config_dr->responseMap['text'][(int)$_GET['status']];
                        $dr_data->closeReason = $config_dr->responseText[(int)$_GET['status']];
                        $dr_data->msgLastStatus = $config_dr->responseMap['text'][(int)$_GET['status']];
        } else {
                        $dr_data->msgStatus = 'FAILED';
                        $dr_data->closeReason = 'FAILED';
                        $dr_data->msgLastStatus = 'FAILED';
        }

        $path = $buffer_file->generate_file_name($dr_data, 'dr', 'dr');

        //var_dump($path, $dr_data);

        if ($buffer_file->save($path, $dr_data)) {
            return $config_dr->returnCode ['OK'];
        } else {
            return $config_dr->returnCode ['NOK'];
        }
    }

    protected function getDRData($mt_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($mt_data)));

        $model_transact = loader_model::getInstance()->load('tblmsgtransact', 'connDatabase1');
        $dataTansact = $model_transact->getDRTransact($mt_data);

        $chargingData = loader_data::get('charging');
        $chargingData->chargingId = $dataTansact[0]['chargingId'];
        $chargingData->senderType = $dataTansact[0]['sender_type'];

        $drData = loader_data::get('dr');
        $drData->msgId = $dataTansact[0]['MSGINDEX'];
        $drData->operatorId = $dataTansact[0]['OPERATORID'];
		$drData->service = $dataTansact[0]['SERVICE'];
        $drData->subject = $dataTansact[0]['SUBJECT'];
        $drData->charging = $chargingData;
        $drData->adn = $dataTansact[0]['ADN'];
        $drData->msisdn = $dataTansact[0]['MSISDN'];
                $this->addPoint($dataTansact,$mt_data);

                return $drData;
    }

    protected function addPoint($dataTansact,$mt_data) {
                $log = manager_logging::getInstance();
                $log->write(array('level' => 'debug', 'message' => 'Start Add Point: ' . serialize($mt_data)));
                $log->write(array('level' => 'debug', 'message' => 'Service '.$dataTansact[0]['SERVICE']));
                if(strtolower($dataTansact[0]['SERVICE'])=='jojoku_9790_pull' && $dataTansact[0]['SUBJECT']=='MT;PULL;SMS;JOJOKUTIPS') {
                        $configDr = loader_config::getInstance()->getConfig('dr');
                        $status = $configDr->responseMap['text'][$mt_data->msgStatus];
                        if($status=='DELIVERED') {
                                $userModel = loader_model::getInstance()->load('user', 'connDatabase1');
                                $user_data = loader_data::get('user');
                                $user_data->active = 0;
                                $user_data->msisdn = $mt_data->msisdn;
                                $user_data->adn = $mt_data->adn;
                                $user_data->service = str_replace('pull','push',$dataTansact[0]['SERVICE']);
                                $user = $userModel->getException($user_data);
                                $log->write(array('level' => 'debug', 'message' => 'User Point'.serialize($user)));
                                if($user) {
                                        $pointModel = loader_model::getInstance()->load('point', 'connDatabase1');
                                        if($pointDt = $pointModel->getAllPoint($user['id'])) {
                                                $point= $pointModel->updatePoint($pointDt['id']);
                                                if($point) {
                                                        $log->write(array('level' => 'debug', 'message' => 'Point succesfully updated'));
                                                }
                                        } else {
                                                $point= $pointModel->insertPoint($user['id']);
                                                if($point) {
                                                        $log->write(array('level' => 'debug', 'message' => 'Point succesfully added'));
                                                }
                                        }
                                }
                        }
                }
        }
		
	private function filterMembership($drData)
	{
		$result = false;
		
		if(strpos(strtoupper($drData->subject, "MT;PULL;SMS;TEXT")) !== FALSE) $result = false;
		
		if(strpos(strtoupper($drData->subject), "MT;PUSH") !== FALSE)
		{
			if(strtoupper($drData->statusInternal) == "DELIVERED")
				$result = true;
			else
				$result = false;
		}
		
		return $result;
	}
	
	private function membership($drData)
	{
		$loader_config = loader_config::getInstance ();
		$configMain = $loader_config->getConfig ( 'main' );
		
		$loader_model = loader_model::getInstance();
		$user = $loader_model->load('user', 'connDatabase1');
				
		$log = manager_logging::getInstance();
		$log->write(array('level' => 'debug', 'message' => 'Start : ' . serialize($drData)));
		
		$trxdata = new stdClass();
		$trxdata->msisdn=$drData->msisdn;
		$trxdata->operator=$configMain->operator;
		$trxdata->service=$drData->service;
				
		$rec = $user->getMembership($trxdata);
		
		if(count($rec) > 0)
		{
			$param = 'msisdn=' . $rec[0]['msisdn'];
			$param .= '&password=' . $rec[0]['password'];
			
			$url = "http://103.44.27.127/tomchat/api/server_reg_request.php";
		
			$hit = http_request::get ( $url, $param, 30 );
		}
		
		return true;
	}
}
