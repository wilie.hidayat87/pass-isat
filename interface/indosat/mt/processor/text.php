<?php

class indosat_mt_processor_text extends default_mt_processor_text {

    private static $instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance) {
            self::$instance = new self ();
        }
        return self::$instance;
    }

    public function saveToQueue($mt_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mt_data)));

       if ($mt_data->charging->messageType == 'mtpush' || (!isset($mt_data->mo->msgId) && empty($mt_data->mo->msgId)) )
        {
            if (empty($mt_data->msgId)) $mt_data->msgId =  date("YmdHis") . str_replace('.', '', microtime(true));
        }
        // Otherwise, take from MO
        else
            $mt_data->msgId = $mt_data->mo->msgId;
return $this->process($mt_data);
}

       public function process($mt) {

	$this->send_mt($mt);
}

    public function send_mt($mt) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start" . $slot));

        $loader_config = loader_config::getInstance();
        $configMT = $loader_config->getConfig('mt');
	$configSDM = $loader_config->getConfig('sdmcode');
        $profile = 'text';
        
        //print_r($queue);
        
            
            //echo('start-----');
            //print_r($mt);
            //http://202.152.162.221:55000/?uid=%UID%&pwd=%PWD%&serviceid=%SID%&msisdn=%MSISDN%&sms=%SMS%&transid=%TRXID%&smstype=0
            //?uid={UID}&pwd={PWD}&serviceid=%s&msisdn=%s&sms=%s&transid=%s&smstype=0
            
            //var_dump($queuePop, $mt);
            
            $param = 'uid=' . $mt->charging->username;
            $param .= '&pwd=' . $mt->charging->password;
            $param .= '&serviceid=' . $mt->charging->chargingId;
            $param .= '&msisdn=' . $mt->msisdn;
			
			// IMPLEMENT 8 ELEMENT
			// Wilie : 2017 - 10 - 17
			// START {
			$sms = $mt->msgData;
			
			if(strtoupper($mt->service) == "MAME_99790_ISAT" && (
				$mt->msisdn == '6285771718827' || $mt->msisdn == '6285798614406'
				))
			{
				$conn = mysql_connect("10.174.11.20", "root", "pu5k0mxxx");
				mysql_select_db("xmp", $conn);

				if(strtoupper($mt->service) == "MAME_99790_ISAT") $service = 'Mame';
					
				$SQL = "SELECT * FROM gamestored WHERE service='".$service."' AND operator = 'indosat' AND date(created_date) = CURRENT_DATE() LIMIT 1";
				$q = mysql_query($SQL);
				$r = mysql_fetch_array($q);
				
				$downloadlink = $r['downloadlink'];
				
				$sms .= " ".$downloadlink;
			}
				
            //******* CUSTOM SMS **********//
			//******* Dev By : Wilie Wahyu H
			//******* Created: 2019-02-19
			$sms = $this->hitCustomSMS($mt, $sms);
			$param .= '&sms=' . urlencode($sms);
			//******* END ****************************//
			// END }
			
            $param .= '&transid=' . $mt->msgId;
            $param .= '&smstype=0';


	        if(isset($configSDM->sdm['SPMSERVICE'][$mt->service]))
	    {
			if($mt->charging->chargingId === '97900287001017' || $mt->charging->chargingId === '97900287006010'){
                                $param .= '&sdmcode=';
                        }else {
				$sdmcode=$configSDM->sdm['SPMSERVICE'][$mt->service];
				$param .= '&sdmcode='. $sdmcode;
			}
	    }

	    $url = $configMT->profile [$profile] ['sendUrl'] [0];
          
			$push = true;
			
			/*
			if(strtoupper($mt->subject) == 'MT;PULL;SMS;TEXT' && (strtoupper($mt->service) <> "JOJOKU_99790_ISAT" || strtoupper($mt->service) <> "VIDEO"))
			{
				$push = false;
				
				if(strpos($sms, 'Anda sudah berhenti') !== FALSE){ $push = true; }
			}
			
			if(strtoupper($mt->subject) == 'MT;PUSH;SMS;TEXT' && (strtoupper($mt->service) <> "JOJOKU_99790_ISAT" || strtoupper($mt->service) <> "VIDEO"))
			{
				$push = false;
				
				if(strpos($sms, 'Anda sudah berhenti') !== FALSE){ $push = true; }
			}
			
			*/
			
			if($push)
			{
				$hit = http_request::get($url, $param, $configMT->profile [$profile] ['SendTimeOut']);
				$log->write ( array ('level' => 'debug', 'message' => "MT Url:" . $url . '?' . $param . ', Result:' . $hit ) );

				$_hit = trim($hit);
				$smsc_response = simplexml_load_string($_hit);
				
				$configDr = loader_config::getInstance()->getConfig('dr');
				if ($configDr->synchrounous === TRUE) {
					if ($_hit == 1) {
						$mt->msgStatus = 'DELIVERED';
					} else {
						$mt->msgStatus = 'FAILED';
					}
					$mt->closeReason = $_hit;
				} else {
					if ($smsc_response->STATUS == 0) {
				//change from $mt->msgStatus = 'DELIVERED'; because when we don't get DR for spesific message, message will be count as revenue, make like PROXL behaviour 
						$mt->msgStatus = '';

					} else {
						$mt->msgStatus = 'FAILED';
					}
					$status = $smsc_response->STATUS;
					$mt->closeReason = $status."|".$configDr->responseACK[(int)$status];
					//var_dump($configDr->responseACK[(int)$status], $smsc_response->STATUS, $mt->closeReason);
				}
			}
            
            //var_dump($url, $param, $_hit);

            
            $mt->msgLastStatus = 'DELIVERED';
            $this->saveMTToTransact($mt);
	    /*
	    if(strtoupper($mt->subject) == "MT;PUSH;SMS;TEXT" && strtolower($mt->operatorName) == "indosat")
	    {
		$class = $mt->operatorName . "_cmp_manager_keyword";
		$CPA = new $class();
		$CPA->process($mt);
	    }
	    */
        return true;
    }

	public function hitCustomSMS($mt, $sms)
	{
		$log = manager_logging::getInstance ();
		
		$loader_config = loader_config::getInstance ();
		$configMT = $loader_config->getConfig ( 'mt' );
		$configMain = $loader_config->getConfig ( 'main' );
		
		if(strpos(strtoupper($mt->subject), 'MT;PUSH') !== FALSE)
		{
			if(strtoupper($mt->service) == "JOJOKU_99790_ISAT")
			{
				$loader_model = loader_model::getInstance();
				$user = $loader_model->load('user', 'connDatabase1');
				
				$trxdata = new stdClass();
				$trxdata->msisdn=$mt->msisdn;
				$trxdata->active=0;
				$trxdata->created_date=date("Y-m-d H:i:s");
				$trxdata->modify_date=date("Y-m-d H:i:s");
				$trxdata->operator=$configMain->operator;
				$trxdata->service=$mt->service;
				$trxdata->password=rand(0001, 9999);
				$trxdata->sent=0;
				
				$rec = $user->getMembership($trxdata);
				
				if(count($rec) > 0)
					$user->updatePasswordMembership($trxdata);
				else
					$user->insertMembership($trxdata);
				
				$sms = str_replace("#userpass#", "user:".$trxdata->msisdn.",pwd:".$trxdata->password, $mt->msgData);
				
				$log->write(array('level' => 'debug', 'message' => 'Response hit password generator ['.$trxdata->msisdn.']: ' . $trxdata->password));
			}
		}
		
		return $sms;
	}
}
