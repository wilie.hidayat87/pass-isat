<?php

class indosat_mt_processor_textdelay extends indosat_mt_processor_text {

    private static $instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new self ();
        }
        return self::$instance;
    }

    public function saveToQueue($mt_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start Save MT to Buffer Delay Indosat: " . serialize($mt_data)));

        // If it's a Push Message or no TrxID found in MO , then the TRXID must be generated
        if ($mt_data->charging->messageType == 'mtpush' || (!isset($mt_data->mo->msgId) && empty($mt_data->mo->msgId)) )
            $mt_data->msgId =  date("YmdHis") . str_replace('.', '', microtime(true));

        // Otherwise, take from MO          
        else
            $mt_data->msgId = $mt_data->mo->msgId;

        
        $mt_delay = new mt_delay_data ();
		$mt_delay->operator = $mt_data->operatorName;
        $mt_delay->service = $mt_data->service;
        $mt_delay->adn = $mt_data->adn;
        $mt_delay->msisdn = $mt_data->msisdn;
        $mt_delay->obj = serialize($mt_data);

		if(!empty($mt_data->delayTime))
			$mt_delay->delayTime = $mt_data->delayTime;
		
        $model_mtdelay = loader_model::getInstance()->load('mtdelay', 'connDatabase1');
        $model_mtdelay->add($mt_delay);

        return true;
    }

}
