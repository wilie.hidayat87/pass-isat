<?php
class xlsdp_broadcast_sentcontent {
	
	public function process(){
    	//test
        $log_profile = 'broadcast';
        $log = manager_logging::getInstance ();
        $log->setProfile($log_profile);
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $config_contentbuffer = loader_config::getInstance ()->getConfig('contentbuffer');
        $slot = $config_contentbuffer->bufferSlot;

        //forking process slot

        for ($i = 0; $i < $slot; $i++) {
            if ($config_contentbuffer->use_forking) {
                switch ($pid = pcntl_fork ()) {
                    case - 1 :
                        $log->write(array('level' => 'error', 'message' => "Forking failed"));
                        die('Forking failed');
                        break;
                    case 0 :
                        $this->processSentContent($i);
                        exit ();
                        break;
                    default :
                        //pcntl_waitpid ( $pid, $status );
                        break;
                }
            } else {
                $this->processSentContent($i);
            }
        }
        return true;
    }
	
	public function processSentContent($slot) 
	{
        $log = manager_logging::getInstance ();
		$log->write(array('level' => 'debug', 'message' => 'Start : ' . $slot));

		$load_config = loader_config::getInstance();
		$buffer_file = buffer_file::getInstance();

		$contentbuffer = $load_config->getConfig('contentbuffer');

		$path = $contentbuffer->bufferPath . '/' . $slot;
		$limit = $contentbuffer->bufferThrottle;
		$result = $buffer_file->read($path, $limit, 'cb');

		if ($result !== false) {
			foreach ($result as $val) {
				foreach ($val as $crDataPath => $data) {
					$log->write(array('level' => 'debug', 'message' => "path : " . $crDataPath . " content : " . serialize($data)));

					if (is_object($data)) 
					{
						$buffer_file->delete($crDataPath);
						
						$crSave = $this->portal_hit($data);
						
					} else {
						$log->write(array('level' => 'error', 'message' => "buffer is not an object"));
						$buffer_file->delete($crDataPath);
					}
				}
			}
		}
    }
	
	public function portal_hit($mt)
	{
		$log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mt)));
		
		$start_hit = time();

		$loader_config = loader_config::getInstance();
        $configContentBuffer = $loader_config->getConfig('contentbuffer');
		
        $url = $configContentBuffer->portalSeruURL . "/pin_history.php";

		$param = 'msisdn=' . $mt->msisdn;
		$param .= '&pin=' . $mt->media;
        $param .= '&date=' . urlencode(date("Y-m-d H:i:s"));

		$hit = http_request::get($url, $param, 10);

		$end_hit = time();

		$log->write ( array ('level' => 'debug', 'message' => 'Result:' . $hit . ', TimeHit='.((int)$end_hit-(int)$start_hit)) );
	}
}
?>