<?php

class config_main {

    public $operator = 'xlsdp';
    public $adn = '99790'; //'3143';
    public $channel = array('sms', 'web', 'wap', 'umb');
    public $partner = array('l7');
    public $prefix = 'xl';
    public $use_forking = true;
    public $request_type = array(
        'confirm' => array('confirm'),
        'reg' => array('reg'),
        'unreg' => array('unreg')
    );

}
