<?php

class config_mt {
    /* server config */

    public $profile = array(
        'default' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpMT',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://localhost:13083/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://localhost:8087/dr/index.php',
            'dlr-mask' => 31
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'text' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpText',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://localhost:13083/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://localhost:8087/dr/index.php',
   	    'dlr-mask' => 31
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'push' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpMTPush',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://localhost:13083/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://localhost:8087/dr/index.php',
            'dlr-mask' => 31
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'wappush' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpMTWapPush',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://localhost:13083/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://localhost:8087/dr/index.php',
            'dlr-mask' => 31
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'optin' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpMTOptin',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://localhost:13083/cgi-bin/sendsms'),
            'dr' => array (
	    'dlr-url' => 'http://localhost:19233/dr/index.php',
            'dlr-mask' => 31
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'dailypush' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpMTDailyPush',
            'slot' => '1',
            'retry' => '5',
            'sendUrl' => array('http://localhost:13083/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://localhost:8087/dr/index.php',
	    'dlr-mask' => 3
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'delaypush' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpMTDelayPush',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://localhost:13083/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://localhost:8087/dr/index.php',
            'dlr-mask' => 31
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        ),
        'delaywappush' => array(
            'type' => 'activemq',
            'protocol' => 'tcp',
            'server' => '127.0.0.1',
            'port' => '61613',
            'prefix' => 'xlsdpDelayWapPush',
            'slot' => '3',
            'retry' => '5',
            'sendUrl' => array('http://localhost:13083/cgi-bin/sendsms'),
            'dr' => array (
            'dlr-url' => 'http://localhost:8087/dr/index.php',
            'dlr-mask' => 31
            ),
            'SendTimeOut' => '10',
            'throttle' => 3
        )
    );


public $service2map = array(
        //'MODEL_9790_PUSH',
        //'jojoku_9790_Push',
        //'sevel_9790_push',
	'JOJOKU_99790_PUSH',
	'SEVEL_99791_PULL',
        'SEVEL_99790_PUSH',
        'MODEL_99790_PUSH',
        //'MODEL_99791_PULL',
        'VIDEO2_99790_PUSH',
	'jojoku',
	'model',
	'seru',
        'seru_99791_pull',
	'nstrpasshallo_push',
	'nstrpasshallo_pull',
	'SERU_99790',
	'SERU_99790_XL',
        'BINTANG_99790_XL',
	'BINTANG_99790',
	'nstrpasshallo_pull'
        //'Video2_9790_Push'
    );

   public $service3map = array(
        'KOIN_HALLO',
        //'CINTA_9979031',
        'SEVEL_99791_PULL',
        'SEVEL_99790_PUSH',
        'MODEL_99790_PUSH',
        //'MODEL_99791_PULL',
        'JOJOKU_99790_PUSH',
	'VIDEO2_99790_PUSH',
        'seru_99791_pull',
	'jojoku',
	'model',
	'seru',
        'JOJO_99790_FT',
	'nstrpasshallo_push',
	'nstrpasshallo_pull',
	'SERU_99790',
	'BINTANG_99790',
	'SERU_99790_XL',
        'BINTANG_99790_XL',
	'nstrpasshallo_pull'
    );

   public $service4map = array(
        'nstrpasshallo',
	'JOJOKU_99790_PUSH',
	'SEVEL_99791_PULL',
        'SEVEL_99790_PUSH',
        'MODEL_99790_PUSH',
        //'MODEL_99791_PULL',
        'VIDEO2_99790_PUSH',
	'jojoku',
	'model',
	'seru',
        'seru_99791_pull',
        'CINTA',
	'nstrpasshallo_push',
	'nstrpasshallo_pull',
	'SERU_99790',
	'BINTANG_99790',
	'SERU_99790_XL',
	'BINTANG_99790_XL',
	'nstrpasshallo_pull'
   );


}
