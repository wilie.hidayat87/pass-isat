<?php

/**
 * 
 * based on XL Axiata configuration
 *
 */
class config_dr {

    public $synchrounous = false; // if TRUE, after MT sent, the DR will be saved, otherwise nothing happens
    
    public $responseMap = array(
    		'text' => array(
    				'0' => 'DELIVERED',
				'1' => 'DELIVERED',
				#'8' => 'DELIVERED',
				'2' => 'FAILED',
				//'8' => 'SENT',
    				'1284' => 'DELIVERED',
    				'0505' => 'REJECTED',
    				'2208' => 'REJECTED',
    				'2100' => 'REJECTED',
    				'3306' => 'REJECTED',
    				'3309' => 'REJECTED',
    		),
    		'push' => array(
    				'0' => 'DELIVERED',
				'1' => 'DELIVERED',
				'2' => 'FAILED',
				//'2' => 'DELIVERED',
				//'8' => 'SENT',
    				'1284' => 'DELIVERED',
    				'0505' => 'REJECTED',
    				'2208' => 'REJECTED',
    				'2100' => 'REJECTED',
    				'3306' => 'REJECTED',
    				'3309' => 'REJECTED',
    		),
    		'wappush' => array(
    				'0' => 'DELIVERED',
				'1' => 'DELIVERED',
				'2' => 'FAILED',
				//'8' => 'SENT',
    				'1284' => 'DELIVERED',
    				'0505' => 'REJECTED',
    				'2208' => 'REJECTED',
    				'2100' => 'REJECTED',
    				'3306' => 'REJECTED',
    				'3309' => 'REJECTED',
    		)
    );
    
    /*
     * 
     * 
     * 1284 – known error code return from SMSC (actually the MT is delivered to end user)
3101 – insufficient balance
0505 – system internal error
2208 – linkID authenticate failed
2100 – service is not exist
3306 – charging failed
3309 – Invalid product‏

    public $responseMap = array(
        'text' => array(
            '0' => 'SEND',
            '1' => 'DELIVERED',
            '2' => 'REJECTED',
            '8' => 'DELIVERED',
            '16' => 'REJECTED',
        ),
        'push' => array(
            '0' => 'SEND',
            '1' => 'DELIVERED',
            '2' => 'REJECTED',
            '8' => 'DELIVERED',
            '16' => 'REJECTED',
        ),
        'wappush' => array(
            '0' => 'SEND',
            '1' => 'DELIVERED',
            '2' => 'REJECTED',
            '8' => 'DELIVERED',
            '16' => 'REJECTED',
        )
    );
    */
    /*
    public $responseMap = array(
        'text' => array(
            '0' => 'SEND',
            '1' => 'DELIVERED',
            '2' => 'REJECTED',
            '8' => 'DELIVERED',
            '16' => 'REJECTED',
			'14' => 'REJECTED',
			'15' => 'REJECTED',
			'10' => 'REJECTED',
			'11' => 'REJECTED',
			'4098' => 'REJECTED',
			'4105' => 'REJECTED',
			'4106' => 'REJECTED',
			'4107' => 'REJECTED',
			'4108' => 'REJECTED',
			'4109' => 'REJECTED',
			'1024' => 'REJECTED',
			'1026' => 'REJECTED',
			'1027' => 'REJECTED',
			'1031' => 'REJECTED',
			'1032' => 'REJECTED',
			'1033' => 'REJECTED',
			'1038' => 'REJECTED',
			'1039' => 'REJECTED',
			'1280' => 'REJECTED',
			'1281' => 'REJECTED',
			'1282' => 'REJECTED',
			'1283' => 'REJECTED',
			'1288' => 'REJECTED',
			'1301' => 'REJECTED',
			'9999' => 'REJECTED'            
        ),
        'push' => array(
            '0' => 'SEND',
            '1' => 'DELIVERED',
            '2' => 'REJECTED',
            '8' => 'DELIVERED',
            '16' => 'REJECTED',
			'14' => 'REJECTED',
			'15' => 'REJECTED',
			'10' => 'REJECTED',
			'11' => 'REJECTED',
			'4098' => 'REJECTED',
			'4105' => 'REJECTED',
			'4106' => 'REJECTED',
			'4107' => 'REJECTED',
			'4108' => 'REJECTED',
			'4109' => 'REJECTED',
			'1024' => 'REJECTED',
			'1026' => 'REJECTED',
			'1027' => 'REJECTED',
			'1031' => 'REJECTED',
			'1032' => 'REJECTED',
			'1033' => 'REJECTED',
			'1038' => 'REJECTED',
			'1039' => 'REJECTED',
			'1280' => 'REJECTED',
			'1281' => 'REJECTED',
			'1282' => 'REJECTED',
			'1283' => 'REJECTED',
			'1288' => 'REJECTED',
			'1301' => 'REJECTED',
			'9999' => 'REJECTED'            
        ),
        'wappush' => array(
            '0' => 'SEND',
            '1' => 'DELIVERED',
            '2' => 'REJECTED',
            '8' => 'DELIVERED',
            '16' => 'REJECTED',
			'14' => 'REJECTED',
			'15' => 'REJECTED',
			'10' => 'REJECTED',
			'11' => 'REJECTED',
			'4098' => 'REJECTED',
			'4105' => 'REJECTED',
			'4106' => 'REJECTED',
			'4107' => 'REJECTED',
			'4108' => 'REJECTED',
			'4109' => 'REJECTED',
			'1024' => 'REJECTED',
			'1026' => 'REJECTED',
			'1027' => 'REJECTED',
			'1031' => 'REJECTED',
			'1032' => 'REJECTED',
			'1033' => 'REJECTED',
			'1038' => 'REJECTED',
			'1039' => 'REJECTED',
			'1280' => 'REJECTED',
			'1281' => 'REJECTED',
			'1282' => 'REJECTED',
			'1283' => 'REJECTED',
			'1288' => 'REJECTED',
			'1301' => 'REJECTED',
			'9999' => 'REJECTED'            
        )
    );
    */
    public $defaultHour = '1';
    public $bufferPath = '/app/xmp2012/buffers/xlsdp/drBuffer';
    public $bufferThrottle = '250';
    public $bufferSlot = 10;
    public $returnCode = array('OK' => 'OK', 'NOK' => 'NOK');
}
