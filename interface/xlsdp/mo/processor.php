<?php

class xlsdp_mo_processor extends default_mo_processor {
	
	public function saveToFile($arrData) {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start : " . serialize ( $arrData ) ) );
		
		$load_config = loader_config::getInstance ();
		$config_mo = $load_config->getConfig ( 'mo' );
		
		$a = explode ( "?", $arrData ['meta'] );
		$msgId = '';
		
		/*
		if (isset ( $a [2] )) {
			parse_str ( $a [2], $aXL );
			if (isset ( $aXL ['TX_ID'] ))
				$msgId = bin2hex ( $aXL ['TX_ID'] );
		}
		// $a = explode("?",$arrData ['meta']);
		// $b = explode("&", $a[2]);
		// $c = explode("=", $b[0]);
		*/
		
		
		$arrData['sms'] = str_replace("%00", "", urlencode($arrData['sms']));
		
		$a = explode ( "?", urldecode($arrData['meta']) );
		if (isset ( $a [2] )) {
			parse_str ( $a [2], $aXL );
			if (isset ( $aXL ['TX_ID'] )) {
				$msgId = $aXL ['TX_ID'];
				$arrData['TX_ID'] = $msgId;
			}				
		}
		
		$mo_data = loader_data::get ( 'mo' );
		//$mo_data->msisdn = $arrData ['msisdn'];
		$mo_data->rawSMS = $arrData ['sms'];
		$mo_data->adn = $arrData ['to'];
		
		$msisdn = str_replace("%2B", "", urlencode($arrData['msisdn']));
		$raw_sms = str_replace("%2B", " ", urlencode($arrData['sms']));

		$mo_data->msisdn = $msisdn;
		$mo_data->rawSMS = $raw_sms;
		
		// $mo_data->msgId = bin2hex($c[1]);
		$mo_data->msgId = $msgId;
		// $arrData ['trx_date']->modify('+7 hours');
		$trxdate = date ( 'Y-m-d H:i:s' );
		$mo_data->incomingDate = $this->setDate ( $trxdate );

		(isset($arrData['channel']) == TRUE) ? $mo_data->channel = $arrData['channel'] : $mo_data->channel = 'sms';

		
		// $mo_data->incomingDate = $this->setDate($arrData ['trx_date']);
		$mo_data->type = 'mtpull';
		
		$buffer_file = buffer_file::getInstance ();
		
		$path = $buffer_file->generate_file_name ( $mo_data );
			
		if ($buffer_file->save ( $path, $mo_data )) {
			return $config_mo->returnCode ['OK'];
		} else {
			return $config_mo->returnCode ['NOK'];
		}
	}
	
	private function setDate($char = '') {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start : " . $char ) );
		
		if (empty ( $char )) {
			return date ( "Y-m-d H:i:s" );
		} else {
			$y = substr ( $char, 0, 4 );
			$m = substr ( $char, 5, 2 );
			$d = substr ( $char, 8, 2 );
			$h = substr ( $char, 11, 2 );
			$i = substr ( $char, 14, 2 );
			$s = substr ( $char, 17, 2 );
			$datetime = $y . '-' . $m . '-' . $d . ' ' . $h . ':' . $i . ':' . $s;
			return $datetime;
		}
	}

}
