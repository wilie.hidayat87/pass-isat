#!/usr/bin/php
<?php

/**
 * 
 * use getopt function to get the parameter. Ex:
 *
 * rpt_partners -t '2014-02-17 17:45:00'  -i '15 minute'
 * 
 * -t : target date 
 * -i : intervals
 * 
 */

require_once('/app/xmp2012/interface/xlsdp/xmp.php');

$params = getopt('t:i:');

if(!isset($params['t']) && !isset($params['i'])) {
	echo 'Incomplete parameter. Usage' . "\n";
	echo "rpt_partners.php -t '2014-02-17 17:45:00'  -i '15 minute' \n";
	exit;
}

$lockFile = '/tmp/lock_xlsdp_rpt_partners';

if(file_exists($lockFile)) {
	echo "NOK - Lock File Exist on $lockFile \n";
	exit;
} else {
	touch($lockFile);
}


$mtManager	= new manager_summarizer();
$result		= $mtManager->rpt_partners($params);

if($result){
	echo "OK \n";
}else{
	echo "NOK \n";
}



unlink($lockFile);
