#!/usr/bin/php
<?php

$params = getopt('s:t:n:');

if(!isset($params['s']) && !isset($params['t'])) {
	echo 'Incomplete parameter. Usage' . "\n";
	echo 'broadcast_queue_sb.php -s game -t 0' . "\n";
	exit(0);
}

require_once '/app/xmp2012/interface/xlsdp/xmp.php';
$broadcast = new manager_broadcast ();
$result = $broadcast->process ();
if ($result) {
	echo "OK \n";
} else {
	echo "NOK \n";
}

exit(0);
