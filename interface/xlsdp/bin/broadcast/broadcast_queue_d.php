#!/usr/bin/php
<?php
/**
 * System_Daemon turns PHP-CLI scripts into daemons.
 *
 * PHP version 5
 *
 * @category  System
 * @package   System_Daemon
 * @author    Kevin <kevin@vanzonneveld.net>
 * @copyright 2008 Kevin van Zonneveld
 * @license   http://www.opensource.org/licenses/bsd-license.php
 * @link      http://github.com/kvz/system_daemon
 */

/**
 * System_Daemon Example Code
 *
 * If you run this code successfully, a daemon will be spawned
 * but unless have already generated the init.d script, you have
 * no real way of killing it yet.
 *
 * In this case wait 3 runs, which is the maximum for this example.
 *
 *
 * In panic situations, you can always kill you daemon by typing
 *
 * killall -9 logparser.php
 * OR:
 * killall -9 php
 *
 */

$runmode = array ('no-daemon' => false, 'help' => false, 'write-initd' => false );

foreach ( $argv as $k => $arg ) {
	if (substr ( $arg, 0, 2 ) == '--' && isset ( $runmode [substr ( $arg, 2 )] )) {
		$runmode [substr ( $arg, 2 )] = true;
	}
}

if ($runmode ['help'] == true) {
	echo 'Usage: ' . $argv [0] . ' [runmode]' . "\n";
	echo 'Available runmodes:' . "\n";
	foreach ( $runmode as $runmod => $val ) {
		echo ' --' . $runmod . "\n";
	}
	die ();
}

ini_set ( 'include_path', ini_get ( 'include_path' ) . ':..' );

error_reporting ( E_STRICT );
require_once 'System/Daemon.php';
//require_once "/usr/share/php/System/Daemon.php";

// Setup
$options = array (
	'appName' => 'xl_brcast_queue', 
	'appDir' => dirname ( __FILE__ ), 
	'appDescription' => 'Broadcast Queue/Sender For XL Daemon', 
	'authorName' => 'idc', 
	'authorEmail' => 'messaging@waki.mobi', 
	'sysMaxExecutionTime' => '0', 
	'sysMaxInputTime' => '0', 
	'sysMemoryLimit' => '1024M', 
	'appRunAsGID' => 503, // 0, // 503, 
	'appRunAsUID' => 501, // 0, // 501,
	'logLocation' => '/app/xmp2012/logs/proxl/daemon/xl_brcast_queue.log' 
);

System_Daemon::setOptions ( $options );

if (! $runmode ['no-daemon']) {
	System_Daemon::start ();
}

if (! $runmode ['write-initd']) {
	System_Daemon::info ( 'not writing an init.d script this time' );
} else {
	if (($initd_location = System_Daemon::writeAutoRun ()) === false) {
		System_Daemon::notice ( 'unable to write init.d script' );
	} else {
		System_Daemon::info ( 'sucessfully written startup script: %s', $initd_location );
	}
}

$runningOkay = true;
$cnt = 1;
$napTime = 20; //60*60; // 60 minutes

//while ( ! System_Daemon::isDying () && $runningOkay && $cnt <= 3 ) {
while ( ! System_Daemon::isDying () && $runningOkay) {
	$mode = '"' . (System_Daemon::isInBackground () ? '' : 'non-') . 'daemon" mode';
	//System_Daemon::info ( '{appName} running in %s %s/3', $mode, $cnt );
	
	//$runningOkay = true;
	$runningOkay = startSending();
	
	if (! $runningOkay) {
		System_Daemon::err ( 'parseLog() produced an error, ' . 'so this will be my last run' );
	}
	System_Daemon::iterate ($napTime);
	
	$cnt ++;
}

// Shut down the daemon nicely
// This is ignored if the class is actually running in the foreground
System_Daemon::stop ();


function startSending() {
	
	$cli_send = "/usr/bin/php -q /app/xmp2012/interface/xlsdp/bin/broadcast/broadcast_queue_sb.php -s %SERVICE% -t %THREAD_ID% -n %LIMIT%";
	$limit = 2000;	
	
	$isQueueExist = isQueueExist(); 
	
	if (isQueueExist()) {
		// swarming
		$services = $isQueueExist[0];
		foreach ($services as $service) {
			for ($i = 0; $i < 10; $i++) {
				$cmd = str_replace('%SERVICE%', $service, $cli_send);
				$cmd = str_replace('%THREAD_ID%', $i, $cmd);
				$cmd = str_replace('%LIMIT%', $limit, $cmd);
				System_Daemon::info ( '{appName} invoking %s', $cmd );
				exec($cmd, $output, $return_val);
			}
		}
	} 
	
	return true;
}

function isQueueExist() {
	
	//System_Daemon::info ( '{appName} running in %s %s/3', $mode, $cnt );
	require_once '/app/xmp2012/interface/xlsdp/xmp.php';
	
	$broadcast_config = loader_config::getInstance ()->getConfig ( 'broadcast' );
	$limit = $broadcast_config->limit;

	$main_config = loader_config::getInstance ()->getConfig ( 'main' );
	$operator_name = $main_config->operator;
	
	$model_operator = loader_model::getInstance ()->load ( 'operator', 'connDatabase1' );
	$operatorId = $model_operator->getOperatorId ( $operator_name );
	
	$mPushProject = loader_model::getInstance ()->load ( 'pushproject', 'connBroadcast' );
	$pushproject_data = new model_data_pushproject ();
	$pushproject_data->created = date ( 'Y-m-d' );
	$pushproject_data->oprid = $operatorId;
	$pushProjects = $mPushProject->get ( $pushproject_data );
		
	if ($pushProjects === false) {
		System_Daemon::info ( '{appName} found no push_project');
		return false;
	} else {
		// push projects
		$services = array();
		$pid_services = array();		
		foreach ( $pushProjects as $pushProject ) {
			array_push($services, $pushProject ['service']);
			$pid_services[$pushProject['service']] = $pushProject ['pid'];
		}
		
		//var_dump(serialize($pid_services));
		//System_Daemon::info ( '{appName} found push_project: %s with pids %s', serialize($services), serialize($pid_services));		
		return array($services, $pid_services);
	}
}