#!/usr/bin/php
<?php
/*
 * Broadcast Queue Dispatcher
 * Need to exec based on cronjob, daemonize cant tell time to execute
 * 
 */

define(CLI_SEND, "/usr/bin/php -q /app/xmp2012/interface/xlsdp/bin/broadcast/broadcast_queue_sb.php -s %SERVICE% -t %THREAD_ID% -n %LIMIT%");
define(LIMIT, 2000);	

function startSending() {
		
	$isQueueExist = isQueueExist(); 
	
	if (isQueueExist()) {
		// swarming
		$services = $isQueueExist[0];
		foreach ($services as $service) {
			for ($i = 0; $i < 10; $i++) {
				$cmd = str_replace('%SERVICE%', $service, $cli_send);
				$cmd = str_replace('%THREAD_ID%', $i, $cmd);
				$cmd = str_replace('%LIMIT%', $limit, $cmd);
				exec($cmd, $output, $return_val);
			}
		}
	} 
	
	return true;
}

function isQueueExist() {
	
	require_once '/app/xmp2012/interface/xlsdp/xmp.php';
	
	$broadcast_config = loader_config::getInstance ()->getConfig ( 'broadcast' );
	$limit = $broadcast_config->limit;

	$main_config = loader_config::getInstance ()->getConfig ( 'main' );
	$operator_name = $main_config->operator;
	
	$model_operator = loader_model::getInstance ()->load ( 'operator', 'connDatabase1' );
	$operatorId = $model_operator->getOperatorId ( $operator_name );
	
	$mPushProject = loader_model::getInstance ()->load ( 'pushproject', 'connBroadcast' );
	$pushproject_data = new model_data_pushproject ();
	$pushproject_data->created = date ( 'Y-m-d' );
	$pushproject_data->oprid = $operatorId;
	$pushProjects = $mPushProject->get ( $pushproject_data );
		
	if ($pushProjects === false) {
		return false;
	} else {
		// push projects
		$services = array();
		$pid_services = array();		
		foreach ( $pushProjects as $pushProject ) {
			array_push($services, $pushProject ['service']);
			$pid_services[$pushProject['service']] = $pushProject ['pid'];
		}
		
		return array($services, $pid_services);
	}
}