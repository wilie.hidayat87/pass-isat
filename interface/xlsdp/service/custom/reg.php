<?php

class proxl_service_custom_reg implements service_listener {

    public function notify($mo_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_data)));

        $mt_processor = new manager_mt_processor ();
        $mo_data->subject = strtoupper('MO;PULL;' . $mo_data->channel . ';REG');
        $mo_data->msgStatus = "DELIVERED";

        $user = loader_data::get('user');
        $user->msisdn = $mo_data->msisdn;
        $user->adn = $mo_data->adn;
        $user->service = $mo_data->service;
        $user->operator = $mo_data->operatorName;
        $user->active = '2';
        $user->partner = $mo_data->partner;

        $user_manager = user_manager::getInstance();
        $subscribe = $user_manager->getUserException($user);

        if ($subscribe !== false)
            $mo_data->partner = $subscribe->partner;

        $mo_data->id = $mt_processor->saveMOToTransact($mo_data);
        if ($subscribe === false) {
            $mo_data->userData = $user;
            $mt = $this->getMsgBeforeRegister($mo_data);
        } else {
            if ($subscribe->active == '1') {
                $mt = $this->getMsgAfterRegistered($mo_data);
            } else {
                $mt = $this->getMsgBeforeRegister($mo_data);
            }
        }
        foreach ($mt as $getMt) {
            $mt_processor->saveToQueue($getMt);
        }
        return $mo_data;
    }

    private function getMsgBeforeRegister($mo_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_data)));

        $service_config = loader_config::getInstance()->getConfig('service');
        $ini_reader = ini_reader::getInstance();
        $ini_data = loader_data::get('ini');
        $ini_data->file = $service_config->iniPath . "reg.ini";

        $arrayMt = array();

        $user = $mo_data->userData;

        $user->transaction_id_subscribe = $mo_data->id;
        $user->channel_subscribe = $mo_data->channel;
        $user->active = '1';
        $user_update = user_manager::getInstance();
        $user_update->addUserData($user);

        $mt = loader_data::get('mt');
        $mt->inReply = $mo_data->id;
        $mt->msgId = $mo_data->msgId;
        $mt->adn = $mo_data->adn;
        $mt->operatorId = $mo_data->operatorId;
        $mt->operatorName = $mo_data->operatorName;
        $mt->service = $mo_data->service;
        $mt->subject = strtoupper('MT;PULL;' . $mo_data->channel . ';REG');
        $mt->msisdn = $mo_data->msisdn;
        $mt->channel = $mo_data->channel;
        $mt->partner = $mo_data->partner;
        $mt->type = 'mtpull';
        $mt->mo = $mo_data;
        $ini_data->type = 'welcome';
        $ini_data->section = 'REPLY';
        $mt->msgData = $ini_reader->get($ini_data);
        $ini_data->section = 'CHARGING';
        $mt->price = $ini_reader->get($ini_data);

        $arrayMt [] = $mt;
        
        $mt2 = loader_data::get('mt');
        $mt2->inReply = $mo_data->id;
        $mt2->msgId = date('YmdHis') . str_replace('.', '', microtime(true));
        $mt2->adn = $mo_data->adn;
        $mt2->operatorId = $mo_data->operatorId;
        $mt2->operatorName = $mo_data->operatorName;
        $mt2->service = $mo_data->service;
        $mt2->subject = strtoupper('MT;PUSH;' . $mo_data->channel . ';SIM');
        $mt2->msisdn = $mo_data->msisdn;
        $mt2->channel = $mo_data->channel;
        $mt2->partner = $mo_data->partner;
        $mt2->type = 'mtpush';
        $mt2->mo = $mo_data;
        $ini_data->type = 'push_after_reg';
        $ini_data->section = 'REPLY';
        $mt2->msgData = $ini_reader->get($ini_data);
        $ini_data->section = 'CHARGING';
        $mt2->price = $ini_reader->get($ini_data);

        $mt_delay = new mt_delay_data ();
        $mt_delay->service = $mo_data->service;
        $mt_delay->adn = $mo_data->adn;
        $mt_delay->msisdn = $mo_data->msisdn;
        $mt_delay->obj = serialize($mt2);

        $model_mtdelay = loader_model::getInstance()->load('mtdelay', 'connDatabase1');
        $model_mtdelay->add($mt_delay);

        return $arrayMt;
    }

    private function getMsgAfterRegistered($mo_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_data)));

        $service_config = loader_config::getInstance()->getConfig('service');
        $ini_reader = ini_reader::getInstance();
        $ini_data = loader_data::get('ini');
        $ini_data->file = $service_config->iniPath . "reg.ini";
        $ini_data->type = 'reg_after_reg';

        $mt = loader_data::get('mt');
        $mt->inReply = $mo_data->id;
        $mt->msgId = $mo_data->msgId;
        $mt->adn = $mo_data->adn;
        $ini_data->section = 'REPLY';
        $mt->msgData = $ini_reader->get($ini_data);
        $ini_data->section = 'CHARGING';
        $mt->price = $ini_reader->get($ini_data);
        $mt->operatorId = $mo_data->operatorId;
        $mt->operatorName = $mo_data->operatorName;
        $mt->service = $mo_data->service;
        $mt->subject = strtoupper('MT;PULL;' . $mo_data->channel . ';REG');
        $mt->msisdn = $mo_data->msisdn;
        $mt->channel = $mo_data->channel;
        $mt->partner = $mo_data->partner;
        $mt->type = 'mtpull';
        $mt->mo = $mo_data;

        return array($mt);
    }

}
