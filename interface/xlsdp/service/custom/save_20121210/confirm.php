<?php

class proxl_service_custom_confirm implements service_listener {

    public function notify($mo_data) {
        $log = manager_logging::getInstance();
        $log->write(array('level' => 'debug', 'message' => "Start : " . serialize($mo_data)));

        $mt_processor = new manager_mt_processor ();
        $mo_data->subject = strtoupper('MO;PULL;' . $mo_data->channel . ';CONFIRM');
        $mo_data->msgStatus = "DELIVERED";

        $mo_data->id = $mt_processor->saveMOToTransact($mo_data);

        $service_config = loader_config::getInstance()->getConfig('service');
        $ini_reader = ini_reader::getInstance();
        $ini_data = loader_data::get('ini');
        $ini_data->file = $service_config->iniPath . "confirm.ini";
        $ini_data->type = 'ack';

        $mt = loader_data::get('mt');
        $mt->inReply = $mo_data->id;
        $mt->msgId = $mo_data->msgId;
        $mt->adn = $mo_data->adn;
        $ini_data->section = 'REPLY';
        $mt->msgData = $ini_reader->get($ini_data);
        $mt->msgData = str_replace('@KEYWORD@', $mo_data->keyword, $mt->msgData);
        $ini_data->section = 'CHARGING';
        $mt->price = $ini_reader->get($ini_data);
        $mt->operatorId = $mo_data->operatorId;
        $mt->operatorName = $mo_data->operatorName;
        $mt->service = $mo_data->service;
        $mt->subject = strtoupper('MT;PULL;' . $mo_data->channel . ';CONFIRM');
        $mt->msisdn = $mo_data->msisdn;
        $mt->channel = $mo_data->channel;
        $mt->partner = $mo_data->partner;
        $mt->type = 'confirm';
        $mt->mo = $mo_data;

        $mt_processor->saveToQueue($mt);

        return $mo_data;
    }

}