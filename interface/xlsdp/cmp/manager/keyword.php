<?php

class xlsdp_cmp_manager_keyword {
	
	// THIS IS FOR PROSES DEFINE PIXEL IN MO
	public function process_pixel($get)
	{
		$log_profile = 'mo_processor';
		$log = manager_logging::getInstance();
		$log->setProfile($log_profile);

		$loaderConfig = loader_config::getInstance();
		
		$data = array();
		$sMsisdn = $get['msisdn'];
		$service = strtolower($get['service']);
		$operator = $get['operator'];

		if($sMsisdn != "0") // msisdn == 0 cannot processed
		{
			//$log->write(array('level' => 'debug', 'message' => "Start : MSISDN : " . $sMsisdn));

			$this->logging("Start : MSISDN : " . $sMsisdn, "process_pixel");

			$isOK = false;

			if($service == 'dg') $isOK = true;
			else if($service == 'video') $isOK = true;
			else if($service == 'video2') $isOK = true;
			else if($service == 'model') $isOK = true;
	
			//$log->write(array('level' => 'debug', 'message' => "Currently service[".$service."] : ".$isOK));
			$this->logging("Currently service[".$service."] : ".$isOK, "process_pixel");

			if($isOK)
			{
				$ps = loader_model::getInstance()->load('hmo', 'cmp');

				$grabPixelData = array(
					"servicename" => $service,
					"operator"    => $operator
				);
				$pixelcode = $ps->getPixel($grabPixelData);
				
				if($pixelcode == "")
				{
					$this->logging("No pixel in pixel storage partner", "process_pixel");
				}
				else	
				{
					if(strlen($pixelcode) == 55) $data['partner']='kimia';
					else $data['partner']='mobusi';

					//$log->write(array('level' => 'debug', 'message' => "Currently partner[".$data['partner']."]"));
					$this->logging("Currently partner[".$data['partner']."]", "process_pixel");

					$data['id']	= $pixelcode;
					$data['msisdn'] = $sMsisdn;
					$data['instid'] = $sMsisdn;
					$config_cmp = loader_config::getInstance()->getConfig('cmp');
					$config_main = loader_config::getInstance()->getConfig('main');

					if(isset($config_cmp->partner[$data['partner']]) && $config_cmp->partner[$data['partner']]==1) 
					{
						$cmp_manager 		= new manager_cmp_processor();
						$data['service']	= $service;
						$data['adn']		= $config_main->adn;
						$data['channel']	= "wap";
						$data['operator']	= $operator;

						ob_start();
						print_r($data);
						$getData = ob_get_clean();

						//$log->write(array('level' => 'debug', 'message' => "data : ".$getData));
						$this->logging("data : ".$getData, "process_pixel");
						
						$cmp_manager->saveToBuffer($data);
						
						//$ps = loader_model::getInstance()->load('hmo', 'cmp');
						//$ps->updatePixelStorage($data);
					}
				}
			}
		}
		else
		{
			//$log->write(array('level' => 'debug', 'message' => "Info : msisdn = 0"));
			$this->logging("Info : msisdn = 0", "process_pixel");
		}
	}
	
	public function process_pixel_2($get)
	{
		$log_profile = 'mo_processor';
		$log = manager_logging::getInstance();
		$log->setProfile($log_profile);

		$loaderConfig = loader_config::getInstance();
		
		$data = array();
		$sMsisdn = $get['msisdn'];
		$service = strtolower($get['service']);
		$operator = $get['operator'];
		$pixelStorageID = $get['pixelStorageID'];

		if($sMsisdn != "0") // msisdn == 0 cannot processed
		{
			$this->logging("Start : MSISDN : " . $sMsisdn . ", Currently service[".$service."]", "process_pixel_2");

			$ps = loader_model::getInstance()->load('hmo', 'cmp');

			$storage = $ps->getPixelSubKeyword(array("pixelStorageID" => $pixelStorageID, "operator" => $operator));
			
			if(count($storage) < 1)
			{
				$this->logging("No pixel in pixel storage partner", "process_pixel_2");
			}
			else	
			{
				$this->logging("Currently partner[".$storage[0]['partner']."]", "process_pixel_2");

				$data['id']	= $storage[0]['pixel'];
				$data['msisdn'] = $sMsisdn;
				$data['instid'] = $sMsisdn;
				$data['partner'] = trim($storage[0]['partner']);
				$config_cmp = loader_config::getInstance()->getConfig('cmp');
				$config_main = loader_config::getInstance()->getConfig('main');

				if(isset($config_cmp->partner[$data['partner']]) && $config_cmp->partner[$data['partner']]==1) 
				{
					$cmp_manager 		= new manager_cmp_processor();
					$data['service']	= strtolower($service);
					$data['adn']		= $config_main->adn;
					$data['channel']	= "wap";
					$data['operator']	= $operator;

					/* ob_start();
					print_r($data);
					$getData = ob_get_clean(); */

					$this->logging("process hmo : ".serialize($data), "process_pixel_2");
					
					$cmp_manager->saveToBuffer($data);
				}
			}
		}
		else
		{
			$this->logging("Info : msisdn = 0", "process_pixel_2");
		}
	}
	
	// THIS IS FOR DR HIT PIXEL WHEN DELIVERED
	public function hit_pixel($data)
	{
		$loaderConfig = loader_config::getInstance();
		$config_cmp = loader_config::getInstance()->getConfig('cmp');
		$config_main = loader_config::getInstance()->getConfig('main');
		
		$ps = loader_model::getInstance()->load('hmo', 'cmp');
		$fetch = $ps->getStorage($data);		
		
		if(count($fetch) > 0)
		{
			$cmp_manager 		= new manager_cmp_processor();
			$data['id']			= $fetch[0]['pixel'];
			$data['msisdn'] 	= $fetch[0]['msisdn'];
			$data['instid'] 	= $fetch[0]['msisdn'];
			$data['service']	= $fetch[0]['servicename'];
			$data['adn']		= $fetch[0]['adn'];
			$data['channel']	= $fetch[0]['channel'];

			ob_start();
			print_r($data);
			$log = ob_get_clean();
			
			$this->logging($log, "hit_pixel");
			
			$cmp_manager->saveToBuffer($data);
		}
		
		return true;
	}
	
	// THIS IS FOR WAP STORAGE PIXEL
	public function pixel_storage($get)
	{
		$log_profile = 'mo_processor';
		$log = manager_logging::getInstance();
		$log->setProfile($log_profile);

		$loaderConfig = loader_config::getInstance();

		$ps = loader_model::getInstance()->load('hmo', 'cmp');
		
		if(strlen(trim($get['pixel'])) == 55) $partner = 'kimia';
		else $partner = 'mobusi';

		$data = array(
			 "pixel"	=> trim($get['pixel'])
			,"operator"	=> trim($get['operator'])
			,"servicename"	=> trim($get['servicename'])
			,"date_time"	=> date("Y-m-d H:i:s")
			,"is_used"	=> "0"
			,"partner"	=> $partner
		);
		
		ob_start();
		print_r($data);
		$log = ob_get_clean();
		
		$this->logging($log, "pixel_storage");

		$ps->savePixelStorage($data);
		return true;
	}

	public function pixel_storage_2($get)
	{	
		//error_log("xlsdp_cmp_manager_keyword \t ps class = ".serialize($get)."  \t" . date("Y-m-d H:i:s") . "\t\r\n", 3, "/tmp/check_core.txt");
		//die;
		$log_profile = 'mo_processor';
		$log = manager_logging::getInstance();
		$log->setProfile($log_profile);

		$loaderConfig = loader_config::getInstance();

		$ps = loader_model::getInstance()->load('hmo', 'cmp');
	
	
		//error_log("xlsdp_cmp_manager_keyword \t after loading core model hmo, ps class = ".serialize($ps)."  \t" . date("Y-m-d H:i:s") . "\t\r\n", 3, "/tmp/check_core.txt");
		//die;
	
		if(strlen(trim($get['pixel'])) == 55) { $partner = 'kimia'; }
		else if(strlen(trim($get['pixel'])) == 23) { $partner = 'mobusi'; }
		else{
			if(substr(strtolower(trim($get['pixel'])), 0, 3) == "102") {
				$partner = 'pocketmedia';
           	}
			else if(strpos(strtolower(trim($get['pixel'])),"bmconv") !== FALSE) { $partner = 'bmb'; }
			else if(strpos(strtolower(trim($get['pixel'])),"test_offer") !== FALSE) { $partner = 'bmb'; }
		}

		$data = array(
			 "pixel"	=> trim($get['pixel'])
			,"operator"	=> trim($get['operator'])
			,"servicename"	=> trim($get['servicename'])
			,"date_time"	=> date("Y-m-d H:i:s")
			,"is_used"	=> "0"
			,"partner"	=> $partner
		);
		
		/* ob_start();
		print_r($data);
		$log = ob_get_clean(); */
		
		$this->logging("save pixel: " . serialize($data), "pixel_storage_2");

		return $ps->savePixelStorageSubKeyword($data);
	}
	
	public function logging($msg, $function)
	{
		//error_log("xlsdp_cmp_manager_keyword  " . $function . "  " . date("Y-m-d H:i:s") . " " . $msg . PHP_EOL, 3, "/app/xmp2012/logs/xlsdp/cpa/cpa-" . date("Y-m-d"));
		error_log("xlsdp_cmp_manager_keyword \t" . $function . "  \t" . date("Y-m-d H:i:s") . "\t$msg\r\n", 3, "/app/xmp2012/logs/xlsdp/cpa/cpa-" . date("Y-m-d"));
	}
}
