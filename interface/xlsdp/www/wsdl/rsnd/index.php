<?php 


require_once '/app/xmp2012/interface/xlsdp/xmp.php';

$xmlstr = file_get_contents('php://input');

// parsing xml
$xml = str_replace(array("soapenv:", "ns1:", "ns2:"),'', $xmlstr);
$data = simplexml_load_string($xml);

response();


// answer section
function response() {
	
	$xmlstring = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sch="http://schema.sag.sdp.huawei.com">
   <soapenv:Header/>
   <soapenv:Body>
      <sch:notifyResendServiceContentRsp>
         <sch:result>
            <!--Optional:-->
            <sch:resultCode>00000000</sch:resultCode>
            <!--Optional:-->
            <sch:resultMessage>Success</sch:resultMessage>
         </sch:result>
         <!--Optional:-->
         <sch:extensionInfo>
            <!--Zero or more repetitions:-->
         </sch:extensionInfo>
      </sch:notifyResendServiceContentRsp>
   </soapenv:Body>
</soapenv:Envelope>
	';
	
	header('Content_type: text/xml');
	echo $xmlstring;
	return true;
}
?>