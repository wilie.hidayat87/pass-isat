<?php 
/*
 * XL SDP Sync Order Relationship Listener
 * @author: idc
 * 
 * 1. get xml and parse
 * 
 * <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soapenv:Body>
      <ns1:syncOrderRelation xmlns:ns1="http://www.csapi.org/schema/parlayx/data/sync/v1_0/local">
         <ns1:userID>
            <ID>2817123455</ID>
            <type>2</type>
         </ns1:userID>
         <ns1:spID>000303</ns1:spID>
         <ns1:productID>1000000631</ns1:productID>
         <ns1:serviceID>0007032000001284</ns1:serviceID>
         <ns1:serviceList>0007032000001284</ns1:serviceList>
         <ns1:updateType>2</ns1:updateType>
         <ns1:updateTime>20130306110956</ns1:updateTime>
         <ns1:updateDesc>Deletion</ns1:updateDesc>
         <ns1:effectiveTime>20130306110955</ns1:effectiveTime>
         <ns1:expiryTime>20130306110956</ns1:expiryTime>
         <ns1:extensionInfo>
            <item>
               <key>payType</key>
               <value>1</value>
            </item>
            <item>
               <key>transactionID</key>
               <value>404090105381303061109540087006</value>
            </item>
            <item>
               <key>orderKey</key>
               <value>999000000000001139</value>
            </item>
            <item>
               <key>chargeMode</key>
               <value>0</value>
            </item>
            <item>
               <key>MDSPSUBEXPMODE</key>
               <value>1</value>
            </item>
            <item>
               <key>objectType</key>
               <value>1</value>
            </item>
            <item>
               <key>isFreePeriod</key>
               <value>false</value>
            </item>
            <item>
               <key>cycleEndTime</key>
               <value>20130405170000</value>
            </item>
            <item>
               <key>durationOfGracePeriod</key>
               <value>3</value>
            </item>
            <item>
               <key>serviceAvailability</key>
               <value>0</value>
            </item>
            <item>
               <key>channelID</key>
               <value>2</value>
            </item>
            <item>
               <key>TraceUniqueID</key>
               <value>404090105381303061109540087007</value>
            </item>
            <item>
               <key>operCode</key>
               <value>in</value>
            </item>
            <item>
               <key>updateReason</key>
               <value>3</value>
            </item>
            <item>
               <key>updateDesc</key>
               <value>7330</value>
            </item>
            <item>
               <key>rentSuccess</key>
               <value>true</value>
            </item>
            <item>
               <key>try</key>
               <value>false</value>
            </item>
         </ns1:extensionInfo>
      </ns1:syncOrderRelation>
   </soapenv:Body>
</soapenv:Envelope>

2. send response

<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:loc="http://www.csapi.org/schema/parlayx/data/sync/v1_0/local">
   <soapenv:Header/>
   <soapenv:Body>
      <loc:syncOrderRelationResponse>
         <loc:result>0</loc:result>
         <loc:resultDescription>OK</loc:resultDescription>
         <!--Optional:-->
         <loc:extensionInfo>
            <!--Zero or more repetitions:-->
            <namedParameters>
               <key>1</key>
               <value>1</value>
            </namedParameters>
         </loc:extensionInfo>
      </loc:syncOrderRelationResponse>
   </soapenv:Body>
</soapenv:Envelope>

3. parse those params and treat as new reg .service. and make mo file


 * 
 */


require_once '/app/xmp2012/interface/xlsdp/xmp.php';

$xmlstr = file_get_contents('php://input');

// parsing xml
$xml = str_replace(array("soapenv:", "ns1:", "ns2:"),'', $xmlstr);
$data = simplexml_load_string($xml);

// trailing
//error_log(date('Ymd His')." : ".print_r($data,1), 3, '/tmp/sdp_reg.log');

if ($data) {
	// translate to array
	$dataArr = array();
	$dataArr['userID']['ID'] = (string)$data->Body->syncOrderRelation->userID->ID;
	$dataArr['userID']['type'] = (string)$data->Body->syncOrderRelation->userID->type;
	$dataArr['spID'] = (string)$data->Body->syncOrderRelation->spID;
	$dataArr['productID'] = (string)$data->Body->syncOrderRelation->productID;
	$dataArr['serviceID'] = (string)$data->Body->syncOrderRelation->serviceID;
	$dataArr['serviceList'] = (string)$data->Body->syncOrderRelation->serviceList;
	$dataArr['updateType']  = (string)$data->Body->syncOrderRelation->updateType;
	$dataArr['updateTime']  = (string)$data->Body->syncOrderRelation->updateTime;
	$dataArr['updateDesc']  = (string)$data->Body->syncOrderRelation->updateDesc;
	$dataArr['effectiveTime']  = (string)$data->Body->syncOrderRelation->effectiveTime;
	$dataArr['expiryTime']  = (string)$data->Body->syncOrderRelation->expiryTime;

	foreach ($data->Body->syncOrderRelation->extensionInfo->item as $key => $value) {

		foreach ((array)$value as $keyname => $keyvalue) {
			if ($keyname == 'key') {
				$temp = $keyvalue;
				$dataArr['extensionInfo'][$temp] = null;
			}
			if ($keyname == 'value') {
				$dataArr['extensionInfo'][$temp] = $keyvalue;
			}
				
		}
			
	}

	// log



	$log = manager_logging::getInstance ();
	$log->setProfile('sdp_request');
	$log->write ( array ('level' => 'debug', 'message' => "SDP Req Sync Order Rel. (sor) :" . serialize($dataArr)) );

	/*
	 * a. cek ini order reg, unreg, modify
	* b. kalo reg check msisdn dulu ada ga, kalo ada replace
	* c. kalo reg dan user ga ada, tambahkan
	* d. kalo unreg kalo user ada deactivate, kalo ga ada no action
	* e. first push
	*
	*/

	$loader_config = loader_config::getInstance();
	$config_main = $loader_config->getConfig('main');

	$xlsdp_functions = new xlsdp_functions();

	//print "<pre>";
	//var_dump($dataArr);

	switch ($dataArr['updateType']) {
		case '1' : // addition
				
			$userdata['msisdn'] = $dataArr['userID']['ID'];
			$userdata['adn'] = $config_main->adn;
			$userdata['service'] = $xlsdp_functions->getServiceName($dataArr['serviceID']);
			$userdata['operator'] = $config_main->operator;
			$userdata['trx_id'] = $dataArr['extensionInfo']['transactionID'];
				
			$res = $xlsdp_functions->addSubscriber($userdata);

			//var_dump($userdata, $res);
			// first push
			if ($res) $fpush = $xlsdp_functions->sendFirstPush($userdata);
				
			break;
		case '2' : // deletion

			$userdata['msisdn'] = $dataArr['userID']['ID'];
			$userdata['adn'] = $config_main->adn;
			$userdata['service'] = $xlsdp_functions->getServiceName($dataArr['serviceID']);
			$userdata['operator'] = $config_main->operator;
			$userdata['trx_id'] = $dataArr['extensionInfo']['transactionID'];
			$res = $xlsdp_functions->removeSubscriber($userdata);
				
			break;
		case '3' : // modification
				
			// ni diisi apa ?, lewat aja deh
				
			break;
	}

	// write wi info to db
	$xldspreglog_data = new model_data_xldspreglog();
	$xldspreglog_data->msisdn =  $userdata['msisdn'];
	$xldspreglog_data->trxid = $dataArr['extensionInfo']['transactionID'];
	$xldspreglog_data->service = $userdata['service'];
	$xldspreglog_data->adn = $userdata['adn'];
	$xldspreglog_data->action = $dataArr['updateType'];
	$xldspreglog_data->act_desc = $dataArr['updateDesc'];
	$xldspreglog_data->operator = $userdata['operator'];
	$xldspreglog_data->channel = $dataArr['extensionInfo']['channelID'];

	$saveRes = $xlsdp_functions->saveRequest($xldspreglog_data);

	answer_sync_order_relshp();

} else {

	$log = manager_logging::getInstance ();
	$log->setProfile('sdp_request');
	$log->write ( array ('level' => 'debug', 'message' => "SDP Sync Order Relshp: Error =>" . serialize($xmlstr)) );

	answer_sync_order_relshp();
}


//============================================================//

function answer_sync_order_relshp() {
	$xmlstring = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:loc="http://www.csapi.org/schema/parlayx/data/sync/v1_0/local">
	<soapenv:Header/>
	<soapenv:Body>
	<loc:syncOrderRelationResponse>
	<loc:result>0</loc:result>
	<loc:resultDescription>OK</loc:resultDescription>
	<!--Optional:-->
	<loc:extensionInfo>
	<!--Zero or more repetitions:-->
	<namedParameters>
	<key>1</key>
	<value>1</value>
	</namedParameters>
	</loc:extensionInfo>
	</loc:syncOrderRelationResponse>
	</soapenv:Body>
	</soapenv:Envelope>';

	header('Content_type: text/xml');
	echo $xmlstring;
	return true;
}

?>
