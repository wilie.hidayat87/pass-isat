<?php 
/*
 * XL SDP Check Service availability test.
 * @author : idc
 *
 *  1. get soap xml and parse
 *  request 
 *  
 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soapenv:Header>
      <ns1:NotifySOAPHeader xmlns:ns1="http://www.huawei.com.cn/schema/common/v2_1">
         <ns1:spRevpassword>741F2C6CC5FAD08AA6DD7C87468CAD3C</ns1:spRevpassword>
         <ns1:spId>000703</ns1:spId>
         <ns1:timeStamp>20130306110825</ns1:timeStamp>
         <ns1:traceUniqueID>404090105381303061108240086008</ns1:traceUniqueID>
      </ns1:NotifySOAPHeader>
   </soapenv:Header>
   <soapenv:Body>
      <ns2:CheckServiceAvailabilityRequest xmlns:ns2="http://schema.sag.sdp.huawei.com">
         <ns2:spID>000703</ns2:spID>
         <ns2:userID>
            <ID>62817123455</ID>
            <type>0</type>
         </ns2:userID>
         <ns2:fakeID>2817123455</ns2:fakeID>
         <ns2:productID>1000000631</ns2:productID>
         <ns2:serviceID>0007032000001284</ns2:serviceID>
      </ns2:CheckServiceAvailabilityRequest>
   </soapenv:Body>
</soapenv:Envelope>

 * expected response
 * 
 *   <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sch="http://schema.sag.sdp.huawei.com">
   <soapenv:Header/>
   <soapenv:Body>
      <sch:CheckServiceAvailabilityResponse>
         <sch:result>00000000</sch:result>
         <!--Optional:-->
         <sch:resultDescription>SUCCESS</sch:resultDescription>
         <!--Optional:-->
         <sch:extensionInfo>
            <!--Zero or more repetitions:-->
            <item>
               <key>result</key>
               <value>OK</value>
            </item>
         </sch:extensionInfo>
      </sch:CheckServiceAvailabilityResponse>
   </soapenv:Body>
</soapenv:Envelope>
 *  
 */

require_once '/app/xmp2012/interface/xlsdp/xmp.php';

$xmlstr = file_get_contents('php://input');

// parsing xml
$xml = str_replace(array("soapenv:", "ns1:", "ns2:"),'', $xmlstr);
$data = simplexml_load_string($xml);

if ($data) {
	// getting parameters
	$csa_params = array();
	$csa_params['spID'] = (string)$data->Body->CheckServiceAvailabilityRequest->spID;
	$csa_params['userID'] = (string)$data->Body->CheckServiceAvailabilityRequest->userID->ID;
	$csa_params['userType'] = (string)$data->Body->CheckServiceAvailabilityRequest->userID->type;
	$csa_params['fakeID'] = (string)$data->Body->CheckServiceAvailabilityRequest->fakeID;
	$csa_params['productID'] = (string)$data->Body->CheckServiceAvailabilityRequest->productID;
	$csa_params['serviceID'] = (string)$data->Body->CheckServiceAvailabilityRequest->serviceID;
	
	$log = manager_logging::getInstance ();
	$log->setProfile('sdp_request');
	$log->write ( array ('level' => 'debug', 'message' => "SDP Req Service Availability:" . serialize($csa_params)) );
	
	$res = answer_service_availability();	
} else {
	$log = manager_logging::getInstance ();
	$log->setProfile('sdp_request');
	$log->write ( array ('level' => 'debug', 'message' => "SDP Req Service Availability: Error =>" . serialize($xmlstr)) );
}

//============================================================//

// answer section
function answer_service_availability() {

	$xmlstring = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sch="http://schema.sag.sdp.huawei.com">
   <soapenv:Header/>
   <soapenv:Body>
      <sch:CheckServiceAvailabilityResponse>
         <sch:result>00000000</sch:result>
         <!--Optional:-->
         <sch:resultDescription>SUCCESS</sch:resultDescription>
         <!--Optional:-->
         <sch:extensionInfo>
            <!--Zero or more repetitions:-->
            <item>
               <key>result</key>
               <value>OK</value>
            </item>
         </sch:extensionInfo>
      </sch:CheckServiceAvailabilityResponse>
   </soapenv:Body>
</soapenv:Envelope>';
	
	header('Content_type: text/xml');
	echo $xmlstring;
	return true;	
}



?>