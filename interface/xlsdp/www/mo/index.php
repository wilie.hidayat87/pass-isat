<?php

require_once '/app/xmp2012/interface/xlsdp/xmp.php';

if (isset($_GET) && count($_GET) != 0) {
	$sms = strtolower($_GET['sms']);
	
	// IF MO is UNREG by default is going trough to mo_processor
	$regMO = true; 
	if(substr($sms, 0, 3) == "reg")
	{
		$regMO = sendToCmpStorage($_GET);
	}
	
	if(substr($sms, 0, 5) == "unreg")
	{		
		$smsArr = explode(" ", trim($sms));
		
		$msisdn = $_GET['msisdn'];
		$servicename = strtolower($smsArr[1]);
		
		$lockPath = '/app/xmp2012/logs/xlsdp/uniq_lock_mo/'.date("Ymd").'_'.$msisdn.'_'.$servicename;
		if(unlink($lockPath)){
			error_log("/mo/index.php " ."\t" . date("Y-m-d H:i:s") . "\t Remove lock file on $lockPath \r\n", 3, "/app/xmp2012/logs/xlsdp/cpa/cpa-" . date("Y-m-d"));
		}
		else{
			error_log("/mo/index.php " ."\t" . date("Y-m-d H:i:s") . "\t Can not remove lock file on $lockPath \r\n", 3, "/app/xmp2012/logs/xlsdp/cpa/cpa-" . date("Y-m-d"));
		}
	}
	
	if($regMO)
	{
		$moProcessor = new manager_mo_processor ( );
		echo $moProcessor->saveToFile($_GET);
	}
	
} else {
    echo 'NOK';
}
//mosdp/index.php?smscid=XLMTSDP&msisdn=1550546071&to=99790&sms=REG+MODEL+1433504769mb85528512345&trx_date=2015-11-05+07:58:39&tid=1446710319&meta=%3Fsmpp%3FTX_ID%3D302152758%26Shortname%3D0046062000014543%26

function sendToCmpStorage($GET) {
  if(strpos(strtolower($GET['sms']), "unreg") === false || strpos(strtolower($GET['sms']), "confirm") === false){
	$datasms = explode(" ", trim($GET['sms']));
    if(count($datasms) > 2)
    {
		list($trigger,$servicename,$identifierP) = explode(" ", trim($GET['sms']));
		
		$msisdn = $GET['msisdn'];
		$servicename = strtolower($servicename);
		
		$lockPath = '/app/xmp2012/logs/xlsdp/uniq_lock_mo/'.date("Ymd").'_'.$msisdn.'_'.$servicename;

		if(file_exists($lockPath)) {
			error_log("/mo/index.php " ."\t" . date("Y-m-d H:i:s") . "\t NOK - Lock File Exist on $lockPath \r\n", 3, "/app/xmp2012/logs/xlsdp/cpa/cpa-" . date("Y-m-d"));
			return false;
		} else {
			touch($lockPath);
	
			$data = array(
				 "msisdn"	=> $msisdn
				,"service"	=> $servicename
				,"operator"	=> "xlsdp"
				,"pixelStorageID" => substr($identifierP, 1, strlen($identifierP))
			);
			
			//$_GET['sms'] = implode(" ", array($trigger, $servicename));
			/*
			ob_start();
			print_r($data);
			$log = ob_get_clean();
			*/
			error_log("/mo/index.php " ."\t" . date("Y-m-d H:i:s") . "\t Start CMP : \t".serialize($data)."\r\n", 3, "/app/xmp2012/logs/xlsdp/cpa/cpa-" . date("Y-m-d"));


			//$fp = fopen("/app/xmp2012/logs/xlsdp/cpa/cpa-treshold-" . date("Y-m-d"), "w");
			
			
			$sPixel = new xlsdp_cmp_manager_keyword();
			$sPixel->process_pixel_2($data);
			
			return true;
		}
	}
	else if(count($datasms) <= 2 && count($datasms) > 1)
    {
		error_log("/mo/index.php " ."\t" . date("Y-m-d H:i:s") . "\t length sms <= 2 && length sms > 1 (".$GET['msisdn'].") \r\n", 3, "/app/xmp2012/logs/xlsdp/cpa/cpa-" . date("Y-m-d"));
		return true;
	}
	else
	{
		error_log("/mo/index.php " ."\t" . date("Y-m-d H:i:s") . "\t length sms < 2 (".$GET['msisdn'].") \r\n", 3, "/app/xmp2012/logs/xlsdp/cpa/cpa-" . date("Y-m-d"));
		
		return false;
	}
  }
}

function sendToCmp($GET) {
	
	$sms = $GET['sms'];

        if(count($arrmbid = explode(' ',$sms))==3) {
                //if(substr(strtolower($sms), 0, 3) &&
		if(substr(strtolower($sms), 0, 3) == "reg" && 
			(strpos(strtolower($sms),'reg model')!==FALSE || 
			strpos(strtolower($sms),'reg video2')!==FALSE)) {
                        /* script ini untuk validasi kimia */
                        // if(preg_match('/^g/',strtolower($arrmbid[2]))){
                        if(strlen($arrmbid[2]) == 55) {
                                $data['partner']='kimia';
                        }
			else if(strlen($arrmbid[2]) == 27) {
                                $data['partner']='appalgo';
                        }
			else {
                               	if(preg_match('/^cd/',strtolower($arrmbid[2]))) {
                                        $data['partner']='cd';
                                        $arrmbid[2] = str_replace('cd','',$arrmbid[2]);
                                } else {
                                        $data['partner']='mobusi';
                                }
                        }
                                $data['id'] = $arrmbid[2];
                                $data['msisdn'] = $GET['msisdn'];
                                $data['instid'] = $GET['msisdn'];
                                $config_cmp = loader_config::getInstance()->getConfig('cmp');
                                if(isset($config_cmp->partner[$data['partner']]) && $config_cmp->partner[$data['partner']]==1) {
                                                $cmp_manager = new manager_cmp_processor();
                                                $data['service']=strtolower($arrmbid[1]);
                                                $data['adn']=$GET['to'];
                                                $cmp_manager->saveToBuffer($data);
                                }
                }
        }

}
