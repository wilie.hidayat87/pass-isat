<?php

class xlsdp_mt_processor_text extends default_mt_processor_text {
	
	private static $instance;
	
	private function __construct() {
	
	}
	
	public static function getInstance() {
		if (! self::$instance) {
			self::$instance = new self ();
		}
		return self::$instance;
	}
	
	private function hex2bin($str) {
		$bin = "";
		$i = 0;
		do {
			$bin .= chr ( hexdec ( $str {$i} . $str {($i + 1)} ) );
			$i += 2;
		} while ( $i < strlen ( $str ) );
		return $bin;
	}
	
	public function saveToQueue($mt_data) {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start : " . serialize ( $mt_data ) ) );
		
		$shortname_model = loader_model::getInstance ()->load ( 'shortname', 'connDatabase1' );
		$shortname_return = $shortname_model->get( $mt_data );
		
		// If it's a Push Message or no TrxID found in MO , then the TRXID must be generated
		if ($mt_data->charging->messageType == 'mtpush' || (! isset ( $mt_data->mo->msgId ) && empty ( $mt_data->mo->msgId ))) {
			if (empty ( $mt_data->msgId ))
				$mt_data->msgId = date ( "YmdHis" ) . str_replace ( '.', '', microtime ( true ) );
		} // Otherwise, take from MO      
		else
			$mt_data->msgId = $mt_data->mo->msgId;
		
		//$mt_data->serviceid = $serviceid['serviceid'];
		$mt_data->shortname = $shortname_return ['value'];
		
		return $this->process ( $mt_data );
	}
	
	public function process($mt) {
		
		// check if mt define as smartcharge-mt
		$smartcharge_f = new smartcharge_functions ();
		$isSmartCharge = $smartcharge_f->isSmartCharge ( $mt );
		if ($isSmartCharge) {
			$this->smartcharge_mt ( $mt, $isSmartCharge );
		} else {
			$this->non_smartcharge ( $mt );
		}
	
	}
	
	public function smartcharge_mt($mt_data, $smartchargeInfo) {
		
		// store in sc table
		$smartcharge_data = new model_data_smartchargetransact ();
		$smartcharge_data->msisdn = $mt_data->msisdn;
		$smartcharge_data->tid = $mt_data->msgId;
		$smartcharge_data->sid = $mt_data->serviceId;
		$smartcharge_data->next_sid = $smartchargeInfo ['next_sid'];
		$smartcharge_data->data = serialize ( $mt_data );
		$smartcharge_data->service = $smartchargeInfo ['service'];
		$smartcharge_data->operator_id = $smartchargeInfo ['operator_id'];
		$smartcharge_data->subject = $smartchargeInfo ['subject'];
		$smartcharge_data->attempt = 0;
		$smartcharge_data->status = 1;
		
		// save
		$smartcharge_f = new smartcharge_functions ();
		$isSmartCharge = $smartcharge_f->save_smartcharge_mt ( $smartcharge_data );
		
		// send mt
		$mt_data->subject = $smartchargeInfo ['subject'];
		$this->send_mt ( $mt_data, true, $smartchargeInfo );
	}
	
	public function non_smartcharge($mt) {
		$this->send_mt ( $mt );
	}
	
	public function send_mt($mt, $smartcharge = false, $smartcharge_info = false) {
		$log = manager_logging::getInstance ();
		if ($smartcharge)
			$log->write ( array ('level' => 'debug', 'message' => "Smartcharge MT." ) );
		else
			$log->write ( array ('level' => 'debug', 'message' => "Non Smartcharge MT." ) );
		
		$loader_config = loader_config::getInstance ();
		$configMT = $loader_config->getConfig ( 'mt' );
		$profile = 'text';
		
		/***
		Change By : Wilie
		Date : 29 Januari 2016	
		***/

		if($mt->charging->chargingId != "9979000")
		{
			$param = 'username=pass';
			$param .= '&password=esemespass';
			$param .= '&smsc=XLMTSDP';
			$param .= '&from=' . $mt->charging->chargingId;
			//$param .= '&charset=UCS-2';
			//$param .= '&coding=2';
			$param .= '&to=' . $mt->msisdn;
		
			if(strtoupper($mt->service) == "SERU_99790_XL")
			{
				if($mt->msisdn == '1908217651' || $mt->msisdn == '1650354441')
				{
					$conn = mysql_connect("10.174.11.20", "root", "pu5k0mxxx");
					mysql_select_db("xmp", $conn);

					if(strtoupper($mt->service) == "SERU_99790_XL") $service = 'Seru';
						
					$SQL = "SELECT * FROM gamestored WHERE service='".$service."' AND operator = 'xl' AND date(created_date) = CURRENT_DATE() LIMIT 1";
					$q = mysql_query($SQL);
					$r = mysql_fetch_array($q);
					
					$downloadlink = $r['downloadlink'];
					
					$sms .= " ".$downloadlink;
				}
			}
			
			// IMPLEMENT 8 ELEMENT
			// Wilie : 2017 - 10 - 17
			// START {

			$sms = $mt->msgData;

			//******* CUSTOM SMS **********//
			//******* Dev By : Wilie Wahyu H
			//******* Created: 2019-02-19

			$sms = $this->hitCustomSMS($mt, $sms);
			
			//******* END ****************************//
			
			$param .= '&text=' . urlencode ( $sms );
			// END }
			
			$metadata = '';
		
			if ($mt->shortname)
				$metadata .= 'Shortname=' . urlencode ( $mt->shortname ) . '&';
		
			//if ($mt->msgId) $metadata .= 'TX_ID='.urlencode($this->hex2bin($mt->msgId));
		
			if ($mt->msgId && strpos ( strtoupper ( $mt->subject ), 'MT;PULL;' ) !== FALSE) {
				$metadata .= 'TX_ID=' . urlencode ($mt->msgId );
				//$metadata .= '&serviceid=' . urlencode ($mt->serviceId);
			}
		
			if (strpos ( strtoupper ( $mt->subject ), 'MT;PUSH;' ) !== FALSE) {
				//$metadata .= 'serviceid=' . urlencode ($mt->serviceid);
			}
		
			if ($metadata)
				$param .= '&meta-data=' . urlencode ( '?smpp?' . $metadata );
		
			$param .= '&dlr-mask=' . $configMT->profile [$profile] ['dr'] ['dlr-mask'];
			$param .= '&dlr-url=' . urlencode ( $configMT->profile [$profile] ['dr'] ['dlr-url'] . '?txid=' . $mt->msgId . '&status=%d&answer=%A&ccode=%P&msisdn=%p&ts=%t&adn=' . $mt->adn .'&meta-data=%D&mdlr=%B&ErrorCode=%B&ErrorSource=%B' );
			if ($smartcharge)
				$param .= urlencode ( '&sc=1&nc=' . $smartcharge_info ['next_sid'] );
		
			$url = $configMT->profile [$profile] ['sendUrl'] [0];
		
			$hit = http_request::get ( $url, $param, $configMT->profile [$profile] ['SendTimeOut'] );
			$log->write ( array ('level' => 'debug', 'message' => "Kannel Url:" . $url . '?' . $param . ', Result:' . $hit ) );
		
			$response = explode ( ':', trim ( $hit ) );
		
			$mt->msgLastStatus = 'DELIVERED';
			$configDr = loader_config::getInstance ()->getConfig ( 'dr' );
			if ($configDr->synchrounous === TRUE) {
				if ($response [0] == '1') {
					$mt->msgStatus = 'DELIVERED';
				} else {
					$mt->msgStatus = 'FAILED';
				}
				$mt->closeReason = $hit;
			} else {
				if ($response [0] != '1') {
					$mt->closeReason = $hit;
				}
			}

			// if (!$smartcharge) $this->saveMTToTransact ( $mt ); // karena write ke rpt_mo juga jadi ga jadi krn nanti ga imbang, instead subject di rubah 
			$this->saveMTToTransact ( $mt );
		
			/*
			// sdp ada delay jadi kudu delat juga dari sini ngirim nya
			$mt_delay = new mt_delay_data ();
			$mt_delay->service = $mt->service;
			$mt_delay->adn = $mt->adn;
			$mt_delay->msisdn = $mt->msisdn;
			$mt_delay->obj = serialize($mt);
		
	
			$model_mtdelay = loader_model::getInstance()->load('mtdelay', 'connDatabase1');
			$model_mtdelay->add($mt_delay);	
			*/

		}

		return true;
	
	}
	
	public function hitCustomSMS($mt, $sms)
	{
		$log = manager_logging::getInstance ();
		
		$loader_config = loader_config::getInstance ();
		$configMT = $loader_config->getConfig ( 'mt' );
		$configMain = $loader_config->getConfig ( 'main' );
		
		if(strpos(strtoupper($mt->subject), 'MT;PUSH') !== FALSE)
		{
			if(strtoupper($mt->service) == "JOJOKU_99790_PUSH" || strtoupper($mt->service) == "VIDEO2_99790_PUSH")
			{
				$loader_model = loader_model::getInstance();
				$user = $loader_model->load('user', 'connDatabase1');
				
				$trxdata = new stdClass();
				$trxdata->msisdn=$mt->msisdn;
				$trxdata->active=0;
				$trxdata->created_date=date("Y-m-d H:i:s");
				$trxdata->modify_date=date("Y-m-d H:i:s");
				$trxdata->operator=$configMain->operator;
				$trxdata->service=$mt->service;
				$trxdata->password=rand(0001, 9999);
				$trxdata->sent=0;
				
				$rec = $user->getMembership($trxdata);
				
				if(count($rec) > 0)
					$user->updatePasswordMembership($trxdata);
				else
					$user->insertMembership($trxdata);
				
				if(strtoupper($mt->service) == "VIDEO2_99790_PUSH")
					$sms = str_replace("#userpass#", "user=".$trxdata->msisdn." & pass=".$trxdata->password, $mt->msgData);
				else
					$sms = str_replace("#userpass#", "user:".$trxdata->msisdn.",pwd:".$trxdata->password, $mt->msgData);

				$log->write(array('level' => 'debug', 'message' => 'Response hit password generator ['.$trxdata->msisdn.']: ' . $trxdata->password));
			}
		}

		$config_contentbuffer = $loader_config->getConfig ( 'contentbuffer' );

		if(strtoupper($mt->service) == "SERU_99790_XL" && $config_contentbuffer->activePortal)
		{
			//if (in_array($mt->msisdn, $config_contentbuffer->serviceSeruWhitelist))
			//{
				if(strpos(strtoupper($mt->subject), ';TEXT') !== FALSE)
				{
					// Set unused Pin to Transact on Field media Service SERU
					$dbconn = mysqli_connect("10.174.11.20", "root", "pu5k0mxxx");
					mysqli_select_db($dbconn,"xmp");
								
					$sql = "SELECT * FROM pin_master WHERE is_used = 0 ORDER BY RAND() LIMIT 1;";
					$query = mysqli_query($dbconn, $sql);
					$rpin = mysqli_fetch_array($query,MYSQLI_ASSOC);
					
					mysqli_query($dbconn, "UPDATE pin_master SET is_used = 1 WHERE pin = '".$rpin['pin']."';");

					mysqli_free_result($query);
					mysqli_close($dbconn);

					$mt->media = $rpin['pin'];
					
					$sms = str_replace(array("#pin#", "#msisdn#"), array($rpin['pin'], $mt->msisdn), $mt->msgData);

					$log->write(array('level' => 'debug', 'message' => 'PIN ['.$mt->msisdn.'] = ' . $rpin['pin']));

					$buffer_file = buffer_file::getInstance ();

					$path = $buffer_file->generate_file_name ( $mt, "contentbuffer", "cb" );
					
					if ($buffer_file->save ( $path, $mt )) {
						$log->write(array('level' => 'debug', 'message' => "Buffer OK, msisdn - " . $mt->msisdn));
					} else {
						$log->write(array('level' => 'debug', 'message' => "Buffer NOK, msisdn - " . $mt->msisdn));
					}
				}

				if(strpos(strtoupper($mt->subject), ';DAILYPUSH') !== FALSE)
				{

				}
			//}
		}
		
		return $sms;
	}

}
