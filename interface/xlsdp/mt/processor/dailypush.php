<?php

class xlsdp_mt_processor_dailypush extends default_mt_processor_text {
	private static $instance;
	
	private function __construct() {
	
	}
	
	public static function getInstance() {
		if (! self::$instance) {
			self::$instance = new self ();
		}
		return self::$instance;
	}
	
	private function hex2bin($str) {
		$bin = "";
		$i = 0;
		do {
			$bin .= chr ( hexdec ( $str {$i} . $str {($i + 1)} ) );
			$i += 2;
		} while ( $i < strlen ( $str ) );
		return $bin;
	}
	
	public function saveToQueue($mt_data) {
		$log = manager_logging::getInstance ();
		$log->write ( array ('level' => 'debug', 'message' => "Start : " . serialize ( $mt_data ) ) );
	
		$shortname_model = loader_model::getInstance ()->load ( 'shortname', 'connDatabase1' );
		$shortname_return = $shortname_model->get ( $mt_data );
	
		// If it's a Push Message or no TrxID found in MO , then the TRXID must be generated
		if ($mt_data->charging->messageType == 'mtpush' || (! isset ( $mt_data->mo->msgId ) && empty ( $mt_data->mo->msgId ))) {
			if (empty ( $mt_data->msgId ))
				$mt_data->msgId = date ( "YmdHis" ) . str_replace ( '.', '', microtime ( true ) );
		} // Otherwise, take from MO
		else
			$mt_data->msgId = $mt_data->mo->msgId;
	
		$mt_data->shortname = $shortname_return ['value'];
	
		return $this->process ( $mt_data );
	}
	
	public function process($mt) {
	
		// check if mt define as smartcharge-mt
		$smartcharge_f = new smartcharge_functions ();
		$isSmartCharge = $smartcharge_f->isSmartCharge ( $mt );
		if ($isSmartCharge) {
			$this->smartcharge_mt ( $mt, $isSmartCharge );
		} else {
			$this->non_smartcharge ( $mt );
		}
	
	}
	
	public function smartcharge_mt($mt_data, $smartchargeInfo) {
	
		// store in sc table
		$smartcharge_data = new model_data_smartchargetransact ();
		$smartcharge_data->msisdn = $mt_data->msisdn;
		$smartcharge_data->tid = $mt_data->msgId;
		$smartcharge_data->sid = $mt_data->serviceId;
		$smartcharge_data->next_sid = $smartchargeInfo ['next_sid'];
		$smartcharge_data->data = serialize ( $mt_data );
		$smartcharge_data->service = $smartchargeInfo ['service'];
		$smartcharge_data->operator_id = $smartchargeInfo ['operator_id'];
		$smartcharge_data->subject = $smartchargeInfo ['subject'];
		$smartcharge_data->attempt = 0;
		$smartcharge_data->status = 1;
	
		// save
		$smartcharge_f = new smartcharge_functions ();
		$isSmartCharge = $smartcharge_f->save_smartcharge_mt ( $smartcharge_data );
	
		// send mt
		$mt_data->subject = $smartchargeInfo ['subject'];
		$this->send_mt ( $mt_data, true, $smartchargeInfo );
	}
	
	public function non_smartcharge($mt) {
		$this->send_mt ( $mt );
	}
	
	public function send_mt($mt, $smartcharge = false, $smartcharge_info = false) {
		$log = manager_logging::getInstance ();
		if ($smartcharge)
			$log->write ( array ('level' => 'debug', 'message' => "Smartcharge MT." ) );
		else
			$log->write ( array ('level' => 'debug', 'message' => "Non Smartcharge MT." ) );
	
		$loader_config = loader_config::getInstance ();
		$configMT = $loader_config->getConfig ( 'mt' );
		$profile = 'text';
	
		$param = 'username=user';
		$param .= '&password=password';
		$param .= '&smsc=XLMTsdp';
		$param .= '&from=' . $mt->charging->chargingId;
		//$param .= '&charset=UCS-2';
		//$param .= '&coding=2';
		$param .= '&to=' . $mt->msisdn;
		$param .= '&text=' . urlencode ( $mt->msgData );
	
		$metadata = '';
		if ($mt->shortname)
			$metadata .= 'Shortname=' . urlencode ( $mt->shortname ) . '&';
	
		//if ($mt->msgId) $metadata .= 'TX_ID='.urlencode($this->hex2bin($mt->msgId));
		if ($mt->msgId && strpos ( strtoupper ( $mt->subject ), 'MT;PULL;' ) !== FALSE)
			$metadata .= 'TX_ID=' . urlencode ( $this->hex2bin ( $mt->msgId ) );
		if ($metadata)
			$param .= '&meta-data=' . urlencode ( '?smpp?' . $metadata );
	
		$param .= '&dlr-mask=' . $configMT->profile [$profile] ['dr'] ['dlr-mask'];
		$param .= '&dlr-url=' . urlencode ( $configMT->profile [$profile] ['dr'] ['dlr-url'] . '?txid=' . $mt->msgId . '&status=%d&answer=%A&ccode=%P&msisdn=%p&ts=%t&adn=' . $mt->adn );
		if ($smartcharge)
			$param .= urlencode ( '&sc=1&nc=' . $smartcharge_info ['next_sid'] );
	
		$url = $configMT->profile [$profile] ['sendUrl'] [0];
	
		$hit = http_request::get ( $url, $param, $configMT->profile [$profile] ['SendTimeOut'] );
		$log->write ( array ('level' => 'debug', 'message' => "Kannel Url:" . $url . '?' . $param . ', Result:' . $hit ) );
	
		$response = explode ( ':', trim ( $hit ) );
	
		$mt->msgLastStatus = 'DELIVERED';
		$configDr = loader_config::getInstance ()->getConfig ( 'dr' );
		if ($configDr->synchrounous === TRUE) {
			if ($response [0] == '1') {
				$mt->msgStatus = 'DELIVERED';
			} else {
				$mt->msgStatus = 'FAILED';
			}
			$mt->closeReason = $hit;
		} else {
			if ($response [0] != '1') {
				$mt->closeReason = $hit;
			}
		}
	
		// if (!$smartcharge) $this->saveMTToTransact ( $mt ); // karena write ke rpt_mo juga jadi ga jadi krn nanti ga imbang, instead subject di rubah
		$this->saveMTToTransact ( $mt );
		return true;
	
	}
}
