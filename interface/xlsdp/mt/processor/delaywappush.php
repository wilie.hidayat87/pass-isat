<?php

class proxl_mt_processor_delaywappush extends default_mt_processor_wappush {

    protected static $instance;

    private function __construct() {

    }

    public static function getInstance() {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance) {
            self::$instance = new self ();
        }
        return self::$instance;
    }

    public function saveToQueue($mt_data) {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        $shortname_model = loader_model::getInstance ()->load('shortname', 'connDatabase1');
        $shortname_return = $shortname_model->get($mt_data);

        $mt_data->shortname = $shortname_return ['value'];

        $loader_queue = loader_queue::getInstance ();
        $loader_config = loader_config::getInstance ();
        $main_config = $loader_config->getConfig('main');
        $mt_config = $loader_config->getConfig('mt');
        $queue = new queue_delay ();
        $data = new queue_data ();

        $profile = 'delaywappush';
        $slot = $mt_config->profile [$profile] ['slot'];
        $lenSlot = strlen($slot);
        $last_num = substr($mt_data->msisdn, - $lenSlot, $lenSlot);
        $num_channel = $last_num % $slot;

        $data->channel = $mt_config->profile [$profile] ['prefix'] . $num_channel;
        $data->value = $mt_data;

        return $queue->put($data);
    }

    public function process($slot) {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));
        
        $profile = "delaywappush";
        $buffer_file = buffer_file::getInstance ();
        $loader_queue = loader_queue::getInstance ();
        $queue = $loader_queue->load($profile);
        
        $queue_delay = new queue_delay ();
        $data = new queue_data ();
        $load_config = loader_config::getInstance ();
        $mt_config = $load_config->getConfig('mt');

        $channel = $mt_config->profile [$profile] ['prefix'] . $slot;

        $queue_delay->setSlot($slot, $channel);
        $file_list = $queue_delay->pop();

        if (!empty($file_list) && $file_list != false) {
            foreach ($file_list as $key => $value) {
                foreach ($value as $mtDatapath => $mt_data) {

                    $mt_data->msgId = date('YmdHis') . str_replace('.', '', microtime(true));
                    $queue_data = $mt_data->msgId . "<TERRY>";
                    $queue_data .= $mt_data->msisdn . "<TERRY>";
                    $queue_data .= "+" . $mt_data->charging->chargingId . "<TERRY>";
                    $queue_data .= $mt_data->msgData . "<TERRY>";
                    $queue_data .= $mt_data->shortname . "<TERRY><TERRY>";

                    $data->channel = $channel;
                    $data->value = $queue_data;
                    $result = $queue->put($data);

                    if ($result === TRUE) {
                        $remove_result = $buffer_file->delete($mtDatapath);
                        $mt_data->msgLastStatus = "success";
                        $mt_data->msgStatus = "success";
                    } else {
                        $mt_data->msgLastStatus = "failed";
                        $mt_data->msgStatus = "failed";
                    }

                    $this->saveMTToTransact($mt_data);
                }
            }
        } else {
            return false;
        }
    }

}