<?php

class xlsdp_mt_processor_push extends default_mt_processor_push {

    private static $instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));

        if (!self::$instance) {
            self::$instance = new self ();
        }
        return self::$instance;
    }

    public function process($slot) {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start : " . $slot));

        $loader_config = loader_config::getInstance ();
        $configMT = $loader_config->getConfig('mt');
        $profile = 'push';
        $queue = loader_queue::getInstance ()->load($profile);
        $queue->subscribe($configMT->profile [$profile] ['prefix'] . $slot);
        
        for ($n = 1; $n <= $configMT->profile [$profile] ['throttle']; $n++) {
            $queuePop = $queue->pop();
            if ($queuePop === false) {
                return false;
            }
            $mt = unserialize($queuePop->body);

            $param = 'cp_name=' . $mt->charging->username;
            $param .= '&pwd=' . $mt->charging->password;
            $param .= '&sid=' . $mt->charging->chargingId;
            $param .= '&msisdn=' . $mt->msisdn;
            $param .= '&sms=' . urlencode($mt->msgData);

            $url = $configMT->profile [$profile] ['sendUrl'] [0];
            foreach ($configMT->profile [$profile] ['sendUrl'] as $urlConfig) {
                preg_match("/(http:\/\/)([A-Za-z0-9-.]*)(:[0-9]{1,5})*/i", $urlConfig, $matches);
                if (!empty($matches [2]))
                    $hostname = $matches [2];
                if (!empty($matches [3]))
                    $hostname .= $matches [3];
                if ($mt->incomingIP == $hostname)
                    $url = $urlConfig;
            }

            $hit = http_request::get($url, $param, $configMT->profile [$profile] ['SendTimeOut']);
            $log->write ( array ('level' => 'debug', 'message' => 'Hit url push : ' . $url . '?' . $param, 'response' => $hit ) );

            $_hit = trim($hit);
            if ($_hit == '1') {
                $mt->msgLastStatus = 'DELIVERED';
                $mt->msgStatus = 'DELIVERED';
            } else {
                $mt->msgLastStatus = 'FAILED';
                $mt->msgStatus = 'FAILED';
            }
            $mt->closeReason = $_hit;

            $this->saveMTToTransact($mt);
        }
        return true;
    } 
    
    
    public function saveToQueue($mt_data) {
        $log = manager_logging::getInstance ();
        $log->write(array('level' => 'debug', 'message' => "Start"));
        
        $shortname_model = loader_model::getInstance ()->load('shortname', 'connDatabase1');
        $shortname_return = $shortname_model->get($mt_data);

        $tmp = $mt_data->mo->msgId;
        $mt_data->msgId = $mt_data->mo->msgIdXL;
        $mt_data->msgIdXL = $tmp;
        $mt_data->shortname = $shortname_return ['value'];
        $loader_queue = loader_queue::getInstance ();
        $loader_config = loader_config::getInstance ();
        $main_config = $loader_config->getConfig('main');
        $mt_config = $loader_config->getConfig('mt');

        $profile = 'push';
        $slot = $mt_config->profile [$profile] ['slot'];
        $lengslot = strlen($slot);
        $last_num = substr($mt_data->msisdn, - $lengslot, $lengslot);
        $num_channel = $last_num % $slot;

        $queue_data = $mt_data->msgId . "<TERRY>";
        $queue_data .= $mt_data->msisdn . "<TERRY>";
        $queue_data .= "+" . $mt_data->charging->chargingId . "<TERRY>";
        $queue_data .= $mt_data->msgData . "<TERRY>";
        $queue_data .= $mt_data->shortname . "<TERRY><TERRY>";
        $queue_data .= $mt_data->msgId;

        $queue = $loader_queue->load($profile);
        $data = new queue_data ();
        $data->channel = $mt_config->profile [$profile] ['prefix'] . $num_channel;
        $data->value = $queue_data;

        $result = $queue->put($data);

        if ($result === FALSE) {
            $mt_data->msgLastStatus = "failed";
            $mt_data->msgStatus = "failed";
        } else {
            $mt_data->msgLastStatus = "success";
            $mt_data->msgStatus = "success";
        }

        $this->saveMTToTransact($mt_data);

        return $result;
    }

}

